<?php

/**

 * @package      CrowdFundingPartners

 * @subpackage   Components

 * @author       Todor Iliev

 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.

 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL

 */



// no direct access

defined('_JEXEC') or die;



/**

 * CrowdFundingPartners Html Helper

 *

 * @package        CrowdFundingPartners

 * @subpackage     Components

 * @since          1.6

 */

abstract class JHtmlCrowdFundingPartners

{

    /**

     * Display an avatar and link to user profile.

     *

     * @param array $partner

     *

     * @return string

     */

    public static function partner($partner)

    {

        $html = array();



        if (!empty($partner["link"])) {

            $html[] = '<a href="'. JRoute::_($partner["link"]) .'"><img src="' . $partner["avatar"] .'" width="32" height="32" class="img-polaroid" /></a>';

        } else {

            //$html[] = '<img src="' . $partner["avatar"] .'" width="32" height="32" class="img-polaroid" />';

        }



        if (!empty($partner["link"])) {

            $html[] = '<a href="'. JRoute::_($partner["link"]) .'">';

            $html[] = htmlentities($partner["prodname"], ENT_QUOTES, "UTF-8");

            $html[] = '</a>';

        } else {

            //$url = urlencode($partner["produrl"]);

            $html[] = "\nProduct Name: ".$partner["prodname"]."</br>Product URL: <a href=".$partner["produrl"].">".$partner["produrl"]."</a></br>Product Price: ".$partner["price"];

        }



        return implode("\n", $html);

    }

}

