<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * CrowdFunding Finance HTML Helper
 *
 * @package		CrowdFundingFinance
 * @subpackage	Components
 * @since		1.6
 */
abstract class JHtmlCrowdFundingFinance {
    
}
