<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="cfshares<?php echo $this->pageclass_sfx; ?>">
    <?php if ($this->params->get('show_page_heading', 1)) { ?>
        <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
    <?php } ?>

    <form action="<?php echo JRoute::_('index.php?option=com_crowdfundingshares&view=shares'); ?>" method="post"
          name="adminForm" id="adminForm">

        <table class="table table-striped table-bordered cfs-shares">
            <thead>
            <tr>
                <th>
                    <?php echo JHtml::_('grid.sort', 'COM_CROWDFUNDINGSHARES_PROJECT', 'b.title', $this->listDirn, $this->listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort', 'COM_CROWDFUNDINGSHARES_SHARES', 'a.shares', $this->listDirn, $this->listOrder); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($this->items as $item) { ?>
                <tr>
                    <td>
                        <a href="<?php echo JRoute::_(CrowdFundingHelperRoute::getDetailsRoute($item->slug, $item->catslug)); ?>">
                            <?php echo JHtmlString::truncate($item->project, 64, true, false); ?>
                        </a>
                    </td>
                    <td class="center has-context">
                        <?php echo $item->shares; ?>
                        <div class="small lower-case">
                            <?php echo $this->escape($item->shares_type); ?>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            </tfoot>
        </table>

        <?php echo $this->pagination->getListFooter(); ?>

        <input type="hidden" name="task" value=""/>
        <input type="hidden" name="filter_order" value="<?php echo $this->listOrder; ?>"/>
        <input type="hidden" name="filter_order_Dir" value="<?php echo $this->listDirn; ?>"/>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>