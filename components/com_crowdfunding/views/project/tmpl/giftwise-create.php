<?php
/**
 * @package      CrowdFunding
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2015 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

if (strcmp("five_steps", $this->wizardType) == 0) {
    $layout      = new JLayoutFile('project_wizard', $this->layoutsBasePath);
} else {
    $layout      = new JLayoutFile('project_wizard_six_steps', $this->layoutsBasePath);
}
echo $layout->render($this->layoutData);
?>

<div class="row-fluid">
    <div class="span6">
        <form action="<?php echo JRoute::_('index.php?option=com_crowdfunding'); ?>" method="post" name="projectForm" id="js-cf-funding-form" novalidate="novalidate" autocomplete="off" enctype="multipart/form-data" >
            
            <?php echo $this->form->getLabel('title'); ?>
            <?php echo $this->form->getInput('title'); ?>
            
            <!--?php echo $this->form->getLabel('short_desc'); ?-->
            <!--?php echo $this->form->getInput('short_desc'); ?-->
            
            <?php echo $this->form->getLabel('catid'); ?>
            <?php echo $this->form->getInput('catid'); ?>

            <!--?php echo $this->form->getLabel('location_preview'); ?-->
            <!--?php echo $this->form->getInput('location_preview'); ?-->
            
            <?php if(!empty($this->numberOfTypes)) {?>
                <?php echo $this->form->getLabel('type_id'); ?>
                <?php echo $this->form->getInput('type_id'); ?>
            <?php  } else { ?>
                <input type="hidden" name="jform[type_id]" value="0" />
            <?php }?>

            
            
            
            
            <?php 
			if($this->params->get("project_terms", 0) AND $this->isNew) {
			    $termsUrl = $this->params->get("project_terms_url", "");
			?>
			<label class="checkbox">
            	<input type="checkbox" name="jform[terms]" value="1" required="required"> <?php echo (!$termsUrl) ? JText::_("COM_CROWDFUNDING_TERMS_AGREEMENT") : JText::sprintf("COM_CROWDFUNDING_TERMS_AGREEMENT_URL", $termsUrl);?>
            </label>
            <?php }?>


            
            <?php echo $this->form->getInput('id'); ?>
            <?php echo $this->form->getInput('location'); ?>


                <div class="span2">
                    <label title="<?php echo JHtml::tooltipText(JText::_("COM_CROWDFUNDING_FIELD_FUNDING_DURATION_DESC"));?>" class="hasTooltip" for="jform_funding_duration_type" id="jform_funding_duration_type-lbl">
                    <?php echo JText::_("COM_CROWDFUNDING_FIELD_FUNDING_DURATION");?><span class="star">&nbsp;*</span>
                    </label>
                </div>
                
                <div class="span10">
                    <?php if(empty($this->fundingDuration) OR (strcmp("days", $this->fundingDuration) == 0)) {?>
                        <input type="radio" value="days" name="jform[funding_duration_type]" id="js-funding-duration-days" <?php echo $this->checkedDays;?>>
                        <?php echo $this->form->getLabel('funding_days'); ?>
                        <div class="clearfix"></div>
                        <?php echo $this->form->getInput('funding_days'); ?>
                        <?php if(!empty($this->maxDays)) {?>
                        <span class="help-block"><?php echo JText::sprintf("COM_CROWDFUNDING_MINIMUM_MAXIMUM_DAYS", $this->minDays, $this->maxDays);?></span>
                        <?php } else {?>
                        <span class="help-block"><?php echo JText::sprintf("COM_CROWDFUNDING_MINIMUM_DAYS", $this->minDays);?></span>
                        <?php }?>
                    <?php }?>
                    
                    <?php if(empty($this->fundingDuration) OR (strcmp("date", $this->fundingDuration) == 0)) {?>
                        <div class="clearfix"></div>
                        <input type="radio" value="date" name="jform[funding_duration_type]" id="js-funding-duration-date" <?php echo $this->checkedDate;?>>            
                        <?php echo $this->form->getLabel('funding_end'); ?>
                        <div class="clearfix"></div>
                        <?php echo $this->form->getInput('funding_end'); ?>
                    <?php }?>
                </div>
          
            
            <input type="hidden" name="task" value="project.save" />
            <?php echo JHtml::_('form.token'); ?>
            
            <div class="clearfix"></div>
            <button type="submit" class="btn mtb_15_0" <?php echo $this->disabledButton;?>>
            	<i class="icon-ok icon-white"></i>
                <?php echo JText::_("COM_CROWDFUNDING_SAVE_AND_CONTINUE")?>
            </button>
        </form>
    </div>
    <?php if($this->imageSmall) {?>
    <div class="span6">
    	<img src="<?php echo $this->imageFolder."/".$this->imageSmall; ?>" class="img-polaroid" />
    	<?php if(!$this->debugMode) {?>
    	<div class="clearfix">&nbsp;</div>
    	<a href="<?php echo JRoute::_("index.php?option=com_crowdfunding&task=project.removeImage&id=".$this->item->id."&".JSession::getFormToken()."=1");?>" class="btn btn-mini btn-danger" >
    		<i class="icon-trash icon-white"></i> 
    		<?php echo JText::_("COM_CROWDFUNDING_REMOVE_IMAGE");?>
		</a>
    	<?php }?>
    </div>
    <?php }?>
</div>