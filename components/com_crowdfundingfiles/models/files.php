<?php
/**
 * @package      CrowdFundingFiles
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Get a list of items
 */
class CrowdFundingFilesModelFiles extends JModelLegacy
{
    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param   string $type    The table type to instantiate
     * @param   string $prefix A prefix for the table class name. Optional.
     * @param   array  $config Configuration array for model. Optional.
     *
     * @return  JTable  A database object
     * @since   1.6
     */
    public function getTable($type = 'File', $prefix = 'CrowdFundingFilesTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function uploadFiles($files, $options)
    {
        $result      = array();
        $destination = JArrayHelper::getValue($options, "destination");
        $maxSize     = JArrayHelper::getValue($options, "max_size");
        $legalExtensions     = JArrayHelper::getValue($options, "legal_extensions");
        $legalFileTypes      = JArrayHelper::getValue($options, "legal_types");

        jimport("itprism.file");
        jimport("itprism.file.image");
        jimport("itprism.file.uploader.local");
        jimport("itprism.file.validator.size");
        jimport("itprism.file.validator.type");
        jimport("itprism.file.validator.server");
        jimport("itprism.string");

        // check for error
        foreach ($files as $fileData) {

            // Upload image
            if (!empty($fileData['name'])) {

                $uploadedFile = JArrayHelper::getValue($fileData, 'tmp_name');
                $uploadedName = JArrayHelper::getValue($fileData, 'name');
                $errorCode    = JArrayHelper::getValue($fileData, 'error');

                $file = new ITPrismFile();

                // Prepare size validator.
                $KB            = 1024 * 1024;
                $fileSize      = JArrayHelper::getValue($fileData, "size");
                $uploadMaxSize = $maxSize * $KB;

                // Prepare file size validator
                $sizeValidator = new ITPrismFileValidatorSize($fileSize, $uploadMaxSize);

                // Prepare server validator.
                $serverValidator = new ITPrismFileValidatorServer($errorCode, array(UPLOAD_ERR_NO_FILE));

                // Prepare image validator.
                $typeValidator = new ITPrismFileValidatorType($uploadedFile, $uploadedName);

                // Get allowed MIME types.
                $mimeTypes = explode(",", $legalFileTypes);
                $mimeTypes = array_map('trim', $mimeTypes);
                $typeValidator->setMimeTypes($mimeTypes);

                // Get allowed file extensions.
                $fileExtensions = explode(",", $legalExtensions);
                $fileExtensions = array_map('trim', $fileExtensions);
                $typeValidator->setLegalExtensions($fileExtensions);

                $file
                    ->addValidator($sizeValidator)
                    ->addValidator($typeValidator)
                    ->addValidator($serverValidator);

                // Validate the file
                if (!$file->isValid()) {
                    throw new RuntimeException($file->getError());
                }

                // Generate file name
                $baseName = JString::strtolower(JFile::makeSafe(basename($fileData['name'])));
                $ext      = JFile::getExt($baseName);

                $generatedName = new ITPrismString();
                $generatedName->generateRandomString(6);

                $destinationFile = $destination . DIRECTORY_SEPARATOR . $generatedName . "." . $ext;

                // Prepare uploader object.
                $uploader = new ITPrismFileUploaderLocal($uploadedFile);
                $uploader->setDestination($destinationFile);

                // Upload temporary file
                $file->setUploader($uploader);

                $file->upload();

                // Get file
                $fileSource = $file->getFile();

                if (!JFile::exists($fileSource)) {
                    throw new RuntimeException(JText::_("COM_CROWDFUNDING_ERROR_FILE_CANT_BE_UPLOADED"));
                }

                $result[] = array(
                    "title"    => $baseName,
                    "filename" => basename($fileSource)
                );
            }
        }

        return $result;
    }

    /**
     * Store the files into database.
     *
     * @param array $files
     * @param int $projectId
     * @param int $userId
     * @param string $fileUri
     *
     * @return array
     */
    public function storeFiles($files, $projectId, $userId, $fileUri)
    {
        settype($files, "array");
        settype($projectId, "integer");
        $result = array();

        if (!empty($files) and !empty($projectId)) {

            $db = JFactory::getDbo();
            /** @var $db JDatabaseDriver */

            foreach ($files as $file) {

                $query = $db->getQuery(true);
                $query
                    ->insert($db->quoteName("#__cffiles_files"))
                    ->set($db->quoteName("title") . "=" . $db->quote($file["title"]))
                    ->set($db->quoteName("filename") . "=" . $db->quote($file["filename"]))
                    ->set($db->quoteName("project_id") . "=" . (int)$projectId)
                    ->set($db->quoteName("user_id") . "=" . (int)$userId);

                $db->setQuery($query);
                $db->execute();

                $lastId = $db->insertid();

                // Add URI path to images
                $result = array(
                    "id"    => $lastId,
                    "title" => $file["title"],
                    "filename" => $file["filename"],
                    "file"  => $fileUri . "/" . $file["filename"]
                );
            }

        }

        return $result;
    }

    /**
     * Delete files.
     *
     * @param integer $fileId File ID
     * @param string  $mediaFolder A path to files folder.
     * @param integer $userId
     *
     * @throws RuntimeException
     */
    public function removeFile($fileId, $mediaFolder, $userId)
    {
        jimport("itprism.file");
        jimport("itprism.file.remover.local");
        jimport("crowdfundingfiles.validator.owner");
        jimport("crowdfundingfiles.file.remover");

        $file = new ITPrismFile();

        // Validate owner of the file.
        $ownerValidator = new CrowdFundingFilesValidatorOwner(JFactory::getDbo(), $fileId, $userId);
        if (!$ownerValidator->isValid()) {
            throw new RuntimeException(JText::_("COM_CROWDFUNDINGFILES_INVALID_FILE"));
        }

        // Remove the file.
        $remover = new CrowdFundingFilesFileRemover(JFactory::getDbo(), $fileId, $mediaFolder);
        $file->addRemover($remover);

        $file->remove();
    }
}
