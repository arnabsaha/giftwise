<?php
/**
* @package      CrowdfundingData
* @subpackage   Library
* @author       Todor Iliev
* @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
* @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

defined('JPATH_PLATFORM') or die;

if (!defined("CROWDFUNDINGDATA_PATH_COMPONENT_ADMINISTRATOR")) {
    define("CROWDFUNDINGDATA_PATH_COMPONENT_ADMINISTRATOR", JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingdata");
}

if (!defined("CROWDFUNDINGDATA_PATH_COMPONENT_SITE")) {
    define("CROWDFUNDINGDATA_PATH_COMPONENT_SITE", JPATH_SITE . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingdata");
}

if (!defined("CROWDFUNDINGDATA_PATH_LIBRARY")) {
    define("CROWDFUNDINGDATA_PATH_LIBRARY", JPATH_LIBRARIES . DIRECTORY_SEPARATOR . "crowdfundingdata");
}

// Register version and constants
JLoader::register("CrowdFundingDataVersion", CROWDFUNDINGDATA_PATH_LIBRARY . DIRECTORY_SEPARATOR . "version.php");

// Register helpers
JLoader::register("CrowdFundingDataHelper", CROWDFUNDINGDATA_PATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . "helpers" . DIRECTORY_SEPARATOR . "crowdfundingdata.php");

// Register HTML helpers
JHtml::addIncludePath(CROWDFUNDINGDATA_PATH_COMPONENT_SITE . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'html');
JLoader::register(
    'JHtmlString',
    JPATH_LIBRARIES . DIRECTORY_SEPARATOR . 'joomla'. DIRECTORY_SEPARATOR .'html'. DIRECTORY_SEPARATOR .'html'. DIRECTORY_SEPARATOR .'string.php'
);