<?php
/**
 * @package      CrowdFundingSharesTransaction
 * @subpackage   Library
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

/**
 * This class provides functionality for managing transactions.
 */
class CrowdFundingSharesTransaction {

    protected $id = 0;
    protected $shares = 0;
    protected $status = "pending";
    protected $record_date;
    protected $type_id = 0;
    protected $transaction_id = 0;
    protected $project_id = 0;
    protected $buyer_id = 0;
    protected $seller_id = 0;

    /**
     * Database driver.
     *
     * @var JDatabaseDriver
     */
    protected $db;

    /**
     * Initialize the object.
     *
     * <code>
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver  $db
     */
    public function __construct(JDatabaseDriver $db = null)
    {
        $this->db = $db;
    }

    /**
     * Set the database object.
     *
     * <code>
     * $transaction    = new CrowdFundingSharesTransaction();
     * $transaction->setDb(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return self
     */
    public function setDb(JDatabaseDriver $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Load transaction data by keys or ID.
     *
     * <code>
     * $keys = array(
     *  "project_id" => 1,
     *  "seller_id" => 2
     * );
     *
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * $transaction->load($keys);
     * </code>
     *
     * @param int|array $keys Reward IDs.
     */
    public function load($keys)
    {
        $query = $this->db->getQuery(true);

        $query
            ->select("a.id, a.shares, a.status, a.record_date, a.type_id, a.transaction_id, a.project_id, a.buyer_id, a.seller_id")
            ->from($this->db->quoteName("#__cfs_transactions", "a"));

        if (!is_array($keys)) {
            $query->where("a.id = " . (int)$keys);
        } else {
            foreach ($keys as $key => $value) {
                $query->where($this->db->quoteName("a.".$key) . "=" . $this->db->quote($value));
            }
        }

        $this->db->setQuery($query);
        $result = $this->db->loadAssoc();

        if (!$result) {
            $result = array();
        }

        $this->bind($result);

    }

    /**
     * Set data to object properties.
     *
     * <code>
     * $data = array(
     *  "shares" => 100,
     *  "project_id" => 1,
     *  "user_id" => 2
     * );
     *
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * $transaction->bind($data);
     * </code>
     *
     * @param array $data
     * @param array $ignored
     */
    public function bind($data, $ignored = array())
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $ignored)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Store data to database.
     *
     * <code>
     * $data = array(
     *  "shares" => 100,
     *  "project_id" => 1,
     *  "buyer_id" => 2,
     *  "seller_id" => 3
     * );
     *
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * $transaction->bind($data);
     * $transaction->store();
     * </code>
     */
    public function store()
    {
        if (!$this->id) { // Insert
            $this->insertObject();
        } else { // Update
            $this->updateObject();
        }
    }

    protected function updateObject()
    {
        $recordDate  = (!$this->record_date) ? "NULL" : $this->db->quote($this->record_date);

        $query = $this->db->getQuery(true);

        $query
            ->update($this->db->quoteName("#__cfs_transactions"))
            ->set($this->db->quoteName("shares") . "=" . $this->db->quote($this->shares))
            ->set($this->db->quoteName("record_date") . "=" . $recordDate)
            ->set($this->db->quoteName("status") . "=" . $this->db->quote($this->status))
            ->set($this->db->quoteName("type_id") . "=" . (int)$this->type_id)
            ->set($this->db->quoteName("transaction_id") . "=" . (int)$this->transaction_id)
            ->set($this->db->quoteName("project_id") . "=" . (int)$this->project_id)
            ->set($this->db->quoteName("buyer_id") . "=" . (int)$this->buyer_id)
            ->set($this->db->quoteName("seller_id") . "=" . (int)$this->seller_id)
            ->where($this->db->quoteName("id") ."=". (int)$this->id);

        $this->db->setQuery($query);
        $this->db->execute();
    }

    protected function insertObject()
    {
        $recordDate  = (!$this->record_date) ? "NULL" : $this->db->quote($this->record_date);

        $query = $this->db->getQuery(true);

        $query
            ->insert($this->db->quoteName("#__cfs_transactions"))
            ->set($this->db->quoteName("shares") . "=" . $this->db->quote($this->shares))
            ->set($this->db->quoteName("record_date") . "=" . $recordDate)
            ->set($this->db->quoteName("status") . "=" . $this->db->quote($this->status))
            ->set($this->db->quoteName("type_id") . "=" . (int)$this->type_id)
            ->set($this->db->quoteName("transaction_id") . "=" . (int)$this->transaction_id)
            ->set($this->db->quoteName("project_id") . "=" . (int)$this->project_id)
            ->set($this->db->quoteName("buyer_id") . "=" . (int)$this->buyer_id)
            ->set($this->db->quoteName("seller_id") . "=" . (int)$this->seller_id);

        $this->db->setQuery($query);
        $this->db->execute();
    }

    /**
     * Update transaction status to database.
     *
     * <code>
     * $keys = array(
     *  "transaction_id" => 1,
     *  "project_id" => 2,
     *  "buyer_id" => 3
     * );
     *
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * $transaction->load($keys);
     * $transaction->setStatus("completed");
     *
     * $transaction->updateStatus();
     * </code>
     */
    public function updateStatus()
    {
        $query = $this->db->getQuery(true);

        $query
            ->update($this->db->quoteName("#__cfs_transactions"))
            ->set($this->db->quoteName("status") . "=" . $this->db->quote($this->status))
            ->where($this->db->quoteName("id") ."=". (int)$this->id);

        $this->db->setQuery($query);
        $this->db->execute();
    }

    /**
     * Set the ID of the user who has bought shares.
     *
     * <code>
     * $buyerId = 1;
     *
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * $transaction->setBuyerId($buyerId);
     * </code>
     *
     * @param int $buyer_id
     *
     * @return self
     */
    public function setBuyerId($buyer_id)
    {
        $this->buyer_id = $buyer_id;

        return $this;
    }

    /**
     * Return the ID of the user who has bought shares.
     *
     * <code>
     * $transactionId = 1;
     *
     * $transaction    = new CrowdFundingSharesTransaction(JFactory::getDbo());
     * $transaction->load($transactionId);
     * $transaction->getBuyerId();
     * </code>
     *
     * @return int
     */
    public function getBuyerId()
    {
        return $this->buyer_id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $projectId
     *
     * @return self
     */
    public function setProjectId($projectId)
    {
        $this->project_id = $projectId;

        return $this;
    }

    /**
     * @return int
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * @return string
     */
    public function getRecordDate()
    {
        return $this->record_date;
    }

    /**
     * @param int $sellerId
     *
     * @return self
     */
    public function setSellerId($sellerId)
    {
        $this->seller_id = $sellerId;

        return $this;
    }

    /**
     * @return int
     */
    public function getSellerId()
    {
        return $this->seller_id;
    }

    /**
     * @param int $shares
     *
     * @return self
     */
    public function setShares($shares)
    {
        $this->shares = $shares;

        return $this;
    }

    /**
     * @return int
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @param int $typeId
     */
    public function setTypeId($typeId)
    {
        $this->type_id = $typeId;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * @param int $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transaction_id = $transactionId;
    }

    /**
     * @return int
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
