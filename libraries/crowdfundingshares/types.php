<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Library
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

jimport("crowdfundingshares.type");

/**
 * This class provides functionality for managing types.
 */
class CrowdFundingSharesTypes implements Iterator, Countable, ArrayAccess
{
    public $types = array();

    /**
     * Database driver.
     *
     * @var JDatabaseDriver
     */
    protected $db;

    protected $position = 0;

    protected static $instance;

    /**
     * Initialize the object.
     *
     * <code>
     * $types    = new CrowdFundingSharesTypes(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db Database object.
     */
    public function __construct(JDatabaseDriver $db = null)
    {
        $this->db = $db;
    }

    public static function getInstance(JDatabaseDriver $db, $options = array())
    {
        if (is_null(self::$instance)) {
            self::$instance = new CrowdFundingSharesTypes($db);
            self::$instance->load($options);
        }

        return self::$instance;
    }

    /**
     * Set a database object.
     *
     * <code>
     * $types    = new CrowdFundingSharesTypes();
     * $types->setDb(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return self
     */
    public function setDb(JDatabaseDriver $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Load types data from database.
     *
     * <code>
     * $types    = new CrowdFundingSharesTypes();
     * $types->setDb(JFactory::getDbo());
     * $types->load();
     * </code>
     *
     */
    public function load()
    {
        $query = $this->db->getQuery(true);

        $query
            ->select("a.id, a.title, a.description, a.params")
            ->from($this->db->quoteName("#__cfs_types", "a"))
            ->order("a.title ASC");

        $this->db->setQuery($query);
        $results = $this->db->loadAssocList();

        if (!empty($results)) {

            foreach ($results as $result) {
                $type = new CrowdFundingSharesType();
                $type->bind($result);
                $this->types[] = $type;
            }

        } else {
            $this->types = array();
        }

    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return (!isset($this->types[$this->position])) ? null : $this->types[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->types[$this->position]);
    }

    public function count()
    {
        return (int)count($this->types);
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->types[] = $value;
        } else {
            $this->types[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->types[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->types[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->types[$offset]) ? $this->types[$offset] : null;
    }
}
