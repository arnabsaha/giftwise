<?php
/**
* @package      CrowdfundingShares
* @subpackage   Library
* @author       Todor Iliev
* @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
* @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

defined('JPATH_PLATFORM') or die;

if (!defined("CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR")) {
    define("CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR", JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingshares");
}

if (!defined("CROWDFUNDINGSHARES_PATH_COMPONENT_SITE")) {
    define("CROWDFUNDINGSHARES_PATH_COMPONENT_SITE", JPATH_SITE . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingshares");
}

if (!defined("CROWDFUNDINGSHARES_PATH_LIBRARY")) {
    define("CROWDFUNDINGSHARES_PATH_LIBRARY", JPATH_LIBRARIES . DIRECTORY_SEPARATOR . "crowdfundingshares");
}

// Register version and constants
JLoader::register("CrowdFundingSharesVersion", CROWDFUNDINGSHARES_PATH_LIBRARY . DIRECTORY_SEPARATOR . "version.php");

// Register helpers
JLoader::register("CrowdFundingSharesHelper", CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . "helpers" . DIRECTORY_SEPARATOR . "crowdfundingshares.php");

// Register HTML helpers
JHtml::addIncludePath(CROWDFUNDINGSHARES_PATH_COMPONENT_SITE . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'html');
JLoader::register(
    'JHtmlString',
    JPATH_LIBRARIES . DIRECTORY_SEPARATOR . 'joomla'. DIRECTORY_SEPARATOR .'html'. DIRECTORY_SEPARATOR .'html'. DIRECTORY_SEPARATOR .'string.php'
);

// Register Observers
JLoader::register(
    "CrowdFundingSharesObserverType",
    CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR .DIRECTORY_SEPARATOR. "tables" .DIRECTORY_SEPARATOR. "observers" .DIRECTORY_SEPARATOR. "type.php"
);
JObserverMapper::addObserverClassToClass('CrowdFundingSharesObserverType', 'CrowdFundingSharesTableType', array('typeAlias' => 'com_crowdfundingshares.type'));
