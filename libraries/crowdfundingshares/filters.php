<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Library
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

/**
 * This class provides functionality for managing filters and options.
 */
class CrowdFundingSharesFilters
{
    protected $options = array();

    /**
     * Database driver.
     *
     * @var JDatabaseMySQLi
     */
    protected $db;

    protected static $instance;

    /**
     * Initialize the object.
     *
     * <code>
     * $filters    = new CrowdFundingSharesFilters(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db Database object.
     */
    public function __construct(JDatabaseDriver $db = null)
    {
        $this->db = $db;
    }

    /**
     * Create an object.
     *
     * <code>
     * $filters    = CrowdFundingSharesFilters::getInstance(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return null|CrowdFundingSharesFilters
     */
    public static function getInstance(JDatabaseDriver $db)
    {
        if (is_null(self::$instance)) {
            self::$instance = new CrowdFundingSharesFilters($db);
        }

        return self::$instance;
    }

    /**
     * Prepare payment statuses as an array with options.
     *
     * <code>
     * $filters    = new CrowdFundingSharesFilters();
     * $options = $filters->getPaymentStatuses();
     * </code>
     *
     * @return array
     */
    public function getPaymentStatuses()
    {
        return array(
            JHtml::_("select.option", "completed", JText::_("COM_CROWDFUNDINGSHARES_COMPLETED")),
            JHtml::_("select.option", "pending", JText::_("COM_CROWDFUNDINGSHARES_PENDING")),
            JHtml::_("select.option", "canceled", JText::_("COM_CROWDFUNDINGSHARES_CANCELED")),
            JHtml::_("select.option", "refunded", JText::_("COM_CROWDFUNDINGSHARES_REFUNDED")),
            JHtml::_("select.option", "failed", JText::_("COM_CROWDFUNDINGSHARES_FAILED"))
        );
    }
    
    /**
     * Load shares types and prepare them as an array with options.
     *
     * <code>
     * $filters = new CrowdFundingSharesFilters(JFactory::getDbo());
     * $options = $filters->getSharesTypes();
     * </code>
     *
     * @return array
     */
    public function getSharesTypes()
    {
        if (!isset($this->options["project_types"])) {

            $query = $this->db->getQuery(true);

            $query
                ->select("a.id AS value, a.title AS text")
                ->from($this->db->quoteName("#__cfs_types", "a"));

            $this->db->setQuery($query);
            $results = $this->db->loadAssocList();

            if (!$results) {
                $results = array();
            }

            $this->options["project_types"] = $results;

        } else {
            $results = $this->options["project_types"];
        }

        return $results;
    }
}
