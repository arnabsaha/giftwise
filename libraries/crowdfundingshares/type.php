<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Library
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

class CrowdFundingSharesType
{
    protected $id = 0;
    protected $title;
    protected $description;
    protected $published = 0;
    protected $params = array();

    /**
     * Database driver.
     *
     * @var JDatabaseDriver
     */
    protected $db;

    /**
     * Initialize the object.
     *
     * <code>
     * $type    = new CrowdFundingSharesType(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return self
     */
    public function __construct(JDatabaseDriver $db = null)
    {
        $this->db = $db;
    }

    /**
     * Set a database object.
     *
     * <code>
     * $typeId  = 1;
     *
     * $type    = new CrowdFundingSharesType();
     * $type->setDb(JFactory::getDbo());
     * $type->load($typeId);
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return self
     */
    public function setDb(JDatabaseDriver $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Load data about type from database.
     *
     * @param int $id Type ID
     *
     * <code>
     * $typeId  = 1;
     *
     * $type    = new CrowdFundingSharesType(JFactory::getDbo());
     * $type->load($typeId);
     * </code>
     */
    public function load($id)
    {
        $query = $this->db->getQuery(true);

        $query
            ->select("a.id, a.title, a.description, a.published, a.params")
            ->from($this->db->quoteName("#__cfs_types", "a"))
            ->where("a.id = " . (int)$id);

        $this->db->setQuery($query);
        $result = $this->db->loadAssoc();

        if (!$result) {
            $result = array();
        }

        $this->bind($result);
    }

    /**
     * Set data to object properties.
     *
     * <code>
     * $data = array(
     *  "title" => "A ticked for...",
     *  "amount" => "10.00"
     * );
     *
     * $type    = new CrowdFundingSharesType();
     * $type->bind($data);
     * </code>
     *
     * @param array $data
     * @param array $ignored
     */
    public function bind($data, $ignored = array())
    {
        foreach ($data as $key => $value) {

            if (!in_array($key, $ignored)) {

                if (strcmp("params", $key) == 0) {
                    $this->setParams($value);
                    continue;
                } else {
                    $this->$key = $value;
                }
            }
        }
    }

    public function setId($id)
    {
        $this->id = (int)$id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function isPublished()
    {
        return ($this->published == 0) ? false : true;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setParams($params)
    {
        if (is_string($params)) {
            $this->params = (array)json_decode($params, true);
        } elseif (is_object($params)) {
            $this->params = JArrayHelper::fromObject($params);
        } elseif (is_array($params)) {
            $this->params = $params;
        } else {
            $this->params = array();
        }

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    /**
     * Return description of the type.
     *
     * <code>
     * $typeId  = 1;
     *
     * $type    = new CrowdFundingSharesType();
     * $type->setDb(JFactory::getDbo());
     * $type->load($typeId);
     *
     * $description = $type->getDescription();
     * </code>
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function isRewardsEnabled()
    {
        $rewards = false;
        if (isset($this->params["rewards"])) {
            $rewards = (!$this->params["rewards"]) ? false : true;
        }

        return $rewards;
    }

    /**
     * Returns an associative array of object properties.
     *
     * <code>
     * $typeId = 1;
     *
     * $type    = new CrowdFundingSharesType();
     * $type->setDb(JFactory::getDbo());
     * $type->load($typeId);
     *
     * $properties = $type->getProperties();
     * </code>
     *
     * @return  array
     */
    public function getProperties()
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $value) {
            if (strcmp("db", $key) == 0) {
                unset($vars[$key]);
            }
        }

        return $vars;
    }
}
