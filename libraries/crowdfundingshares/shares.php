<?php
/**
 * @package      CrowdFundingSharesShares
 * @subpackage   Library
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

/**
 * This class provides functionality for managing a virtual shares.
 */
class CrowdFundingSharesShares
{
    protected $id = 0;
    protected $shares = 0;
    protected $type_id = 0;
    protected $project_id = 0;
    protected $user_id = 0;

    /**
     * Database driver.
     *
     * @var JDatabaseDriver
     */
    protected $db;

    /**
     * Initialize the object.
     *
     * <code>
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver  $db
     */
    public function __construct(JDatabaseDriver $db = null)
    {
        $this->db = $db;
    }

    /**
     * Set the database object.
     *
     * <code>
     * $shares    = new CrowdFundingSharesShares();
     * $shares->setDb(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return self
     */
    public function setDb(JDatabaseDriver $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Load shares data by keys or ID.
     *
     * <code>
     * $keys = array(
     *  "project_id" => 1,
     *  "user_id" => 2
     * );
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($keys);
     * </code>
     *
     * @param int|array $keys Reward IDs.
     */
    public function load($keys)
    {
        $query = $this->db->getQuery(true);

        $query
            ->select("a.id, a.shares, a.type_id, a.project_id, a.user_id")
            ->from($this->db->quoteName("#__cfs_shares", "a"));

        if (!is_array($keys)) {
            $query->where("a.id = " . (int)$keys);
        } else {
            foreach ($keys as $key => $value) {
                $query->where($this->db->quoteName("a.".$key) . "=" . $this->db->quote($value));
            }
        }

        $this->db->setQuery($query);
        $result = $this->db->loadAssoc();

        if (!$result) {
            $result = array();
        }

        $this->bind($result);

    }

    /**
     * Set data to object properties.
     *
     * <code>
     * $data = array(
     *  "shares" => 100,
     *  "project_id" => 1,
     *  "user_id" => 2
     * );
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->bind($data);
     * </code>
     *
     * @param array $data
     * @param array $ignored
     */
    public function bind($data, $ignored = array())
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $ignored)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Store data to database.
     *
     * <code>
     * $data = array(
     *  "shares" => 100,
     *  "project_id" => 1,
     *  "user_id" => 2
     * );
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->bind($data);
     * $shares->store();
     * </code>
     */
    public function store()
    {
        if (!$this->id) { // Insert
            $this->insertObject();
        } else { // Update
            $this->updateObject();
        }
    }

    protected function updateObject()
    {
        $query = $this->db->getQuery(true);

        $query
            ->update($this->db->quoteName("#__cfs_shares"))
            ->set($this->db->quoteName("shares") . "=" . $this->db->quote($this->shares))
            ->set($this->db->quoteName("user_id") . "=" . (int)$this->user_id)
            ->set($this->db->quoteName("project_id") . "=" . (int)$this->project_id)
            ->set($this->db->quoteName("type_id") . "=" . (int)$this->type_id)
            ->where($this->db->quoteName("id") ."=". (int)$this->id);

        $this->db->setQuery($query);
        $this->db->execute();
    }

    protected function insertObject()
    {
        $query = $this->db->getQuery(true);

        $query
            ->insert($this->db->quoteName("#__cfs_shares"))
            ->set($this->db->quoteName("shares") . "=" . $this->db->quote($this->shares))
            ->set($this->db->quoteName("user_id") . "=" . (int)$this->user_id)
            ->set($this->db->quoteName("project_id") . "=" . (int)$this->project_id)
            ->set($this->db->quoteName("type_id") . "=" . (int)$this->type_id);

        $this->db->setQuery($query);
        $this->db->execute();
    }

    /**
     * Store the number of shares to database.
     *
     * <code>
     * $keys = array(
     *  "user_id" => 1,
     *  "project_id" => 2,
     *  "type_id" => 3
     * );
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($keys);
     *
     * $shares->increase(50);
     *
     * $shares->updateShares();
     * </code>
     */
    public function updateShares()
    {
        $query = $this->db->getQuery(true);

        $query
            ->update($this->db->quoteName("#__cfs_shares"))
            ->set($this->db->quoteName("shares") . "=" . $this->db->quote($this->shares))
            ->where($this->db->quoteName("id") ."=". (int)$this->id);

        $this->db->setQuery($query);
        $this->db->execute();
    }

    /**
     * Increase the number of shares.
     *
     * <code>
     * $recordId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($recordId);
     * $shares->increase(10);
     * $shares->store();
     * </code>
     *
     * @param integer $number
     */
    public function increase($number)
    {
        if ($number > 0) {
            $this->shares = $this->shares + abs($number);
        }

    }

    /**
     * Decrease the number of shares.
     *
     * <code>
     * $recordId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($recordId);
     * $shares->decrease(10);
     * $shares->store();
     * </code>
     *
     * @param integer $number
     */
    public function decrease($number)
    {
        if (($number > 0) and ($this->shares >= $number)) {
            $this->shares = $this->shares - abs($number);
        }
    }

    /**
     * Return shares record ID.
     *
     * <code>
     * $keys = array(
     *  "project_id" => 1,
     *  "user_id" => 2
     * );
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($keys);
     *
     * if (!$shares->getId()) {
     * ....
     * }
     * </code>
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * Set the ID of the project.
     *
     * <code>
     * $projectId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->setProjectId($projectId);
     * </code>
     *
     * @param int $projectId
     *
     * @return self
     */
    public function setProjectId($projectId)
    {
        $this->project_id = $projectId;

        return $this;
    }

    /**
     * Return the ID of the project.
     *
     * <code>
     * $itemId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($itemId);
     *
     * $projectId = $shares->getProjectId()
     * </code>
     *
     * @return int
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Return the number of shares.
     *
     * <code>
     * $itemId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($itemId);
     *
     * $numberOfShares = $shares->getShares()
     * </code>
     *
     * @return int
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * Set the ID of the user.
     *
     * <code>
     * $userId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->setUserId($userId);
     * </code>
     *
     * @param int $userId
     *
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * Return the ID of the user.
     *
     * <code>
     * $itemId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($itemId);
     *
     * $userId = $shares->getUserId()
     * </code>
     *
     * @return int
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     * Set the ID of a shares type.
     *
     * <code>
     * $typeId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->setTypeId($typeId);
     * </code>
     *
     * @param int $typeId
     *
     * @return self
     */
    public function setTypeId($typeId)
    {
        $this->type_id = $typeId;
        return $this;
    }

    /**
     * Return the ID of the shares type.
     *
     * <code>
     * $itemId = 1;
     *
     * $shares    = new CrowdFundingSharesShares(JFactory::getDbo());
     * $shares->load($itemId);
     *
     * $typeId = $shares->getTypeId()
     * </code>
     *
     * @return int
     */
    public function getTypeId()
    {
        return (int)$this->type_id;
    }
}
