<?php

/**

 * @package      CrowdFundingPartners

 * @subpackage   Partners

 * @author       Todor Iliev

 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.

 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL

 */



defined('JPATH_PLATFORM') or die;



/**

 * This class provides functionality that manage records.

 *

 * @package      CrowdFundingPartners

 * @subpackage   Partners

 */

class CrowdFundingPartnersPartner

{

    protected $id;

    protected $name;

    protected $project_id;

    protected $partner_id;



    /**

     * Database driver.

     *

     * @var JDatabaseDriver

     */

    protected $db;



    /**

     * Initialize the object.

     *

     * <code>

     * $partner    = new CrowdFundingPartnersPartner(JFactory::getDbo());

     * </code>

     *

     * @param JDatabaseDriver  $db

     */

    public function __construct(JDatabaseDriver $db = null)

    {

        $this->db = $db;

    }



    /**

     * Load data of a record from database.

     *

     * <code>

     * $id = 1;

     *

     * $partner    = new CrowdFundingPartnersPartner();

     * $partner->setDb(JFactory::getDbo());

     * $partner->load($id);

     * </code>

     *

     * @param int $id

     */

    public function load($id)

    {

        $query = $this->db->getQuery(true);



        $query

            ->select(

                "a.id, a.prodname, a.project_id, a.user_id, a.produrl, a.price"

            )

            ->from($this->db->quoteName("#__cfpartners_partners", "a"))

            ->where("a.id = " .(int)$id);



        $this->db->setQuery($query);

        $result = $this->db->loadAssoc();



        if (!$result) {

            $result = array();

        }



        $this->bind($result);

    }



    /**

     * Set data to object properties.

     *

     * <code>

     * $data = array(

     *  "name"       => "John Dow",

     *  "partner_id" => 1

     * );

     *

     * $partner    = new CrowdFundingPartnersPartner();

     * $partner->bind($data);

     * </code>

     *

     * @param array $data

     * @param array $ignored

     */

    public function bind($data, $ignored = array())

    {

        foreach ($data as $key => $value) {

            if (!in_array($key, $ignored)) {

                $this->$key = $value;

            }

        }

    }



    /**

     * Return record ID.

     *

     * <code>

     * $id  = 1;

     *

     * $partner    = new CrowdFundingPartnersPartner(JFactory::getDbo());

     * $partner->load($id);

     *

     * if (!$partner->getId()) {

     * ...

     * }

     * </code>

     *

     * @return int

     */

    public function getId()

    {

        return $this->id;

    }



    /**

     * Return an ID of a user, that has been assigned as a partner to a project.

     *

     * <code>

     * $id  = 1;

     *

     * $partner    = new CrowdFundingPartnersPartner(JFactory::getDbo());

     * $partner->load($id);

     *

     * $partnerId = $partner->getPartnerId();

     * </code>

     *

     * @return int

     */

    public function getPartnerId()

    {

        return $this->partner_id;

    }



    /**

     * Return a name of an user, that has been assigned as a partner to a project.

     *

     * <code>

     * $id  = 1;

     *

     * $partner    = new CrowdFundingPartnersPartner(JFactory::getDbo());

     * $partner->load($id);

     *

     * $name = $partner->getName();

     * </code>

     *

     * @return string

     */

    public function getName()

    {

        return $this->name;

    }



    /**

     * Return a project ID.

     *

     * <code>

     * $id  = 1;

     *

     * $partner    = new CrowdFundingPartnersPartner(JFactory::getDbo());

     * $partner->load($id);

     *

     * $projectId = $partner->getProjectId();

     * </code>

     *

     * @return string

     */

    public function getProjectId()

    {

        return $this->project_id;

    }

    

    /**

     * Returns an associative array of object properties.

     *

     * <code>

     * $id = 1;

     *

     * $partner    = new CrowdFundingPartnersPartner();

     * $partner->setDb(JFactory::getDbo());

     * $partner->load($id);

     *

     * $properties = $partner->getProperties();

     * </code>

     *

     * @return  array

     */

    public function getProperties()

    {

        $vars = get_object_vars($this);



        foreach ($vars as $key => $value) {

            if (strcmp("db", $key) == 0) {

                unset($vars[$key]);

                break;

            }

        }



        return $vars;

    }

}

