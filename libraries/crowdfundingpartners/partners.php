<?php

/**

 * @package      CrowdFundingPartners

 * @subpackage   Files

 * @author       Todor Iliev

 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.

 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL

 */



defined('JPATH_PLATFORM') or die;



/**

 * This class provides functionality that manage partners.

 *

 * @package      CrowdFundingPartners

 * @subpackage   Parnters

 */

class CrowdFundingPartnersPartners implements Iterator, Countable, ArrayAccess

{

    protected $items = array();



    protected $position = 0;



    /**

     * Database driver.

     *

     * @var JDatabaseDriver

     */

    protected $db;



    /**

     * Initialize the object.

     *

     * <code>

     * $partners   = new CrowdFundingPartnersPartners(JFactory::getDbo());

     * </code>

     *

     * @param JDatabaseDriver $db

     */

    public function __construct(JDatabaseDriver $db)

    {

        $this->db = $db;

    }



    /**

     * Load partners data by ID from database.

     *

     * <code>

     * $ids = array(1,2,3,4,5);

     *

     * $partners   = new CrowdFundingPartnersPartners(JFactory::getDbo());

     * $partners->load($ids);

     *

     * foreach($partners as $partner) {

     *   echo $partners["name"];

     *   echo $partners["partner_id"];

     * }

     *

     * </code>

     *

     * @param int $projectId

     * @param array $ids

     */

    public function load($projectId = 0, $ids = array())

    {

        // Load project data

        $query = $this->db->getQuery(true);



        $query

            ->select("a.id, a.prodname, a.project_id, a.user_id, a.produrl, a.price")

            ->from($this->db->quoteName("#__cfpartners_partners", "a"));



        if (!empty($ids)) {

            JArrayHelper::toInteger($ids);

            $query->where("a.id IN ( " . implode(",", $ids) . " )");

        }



        if (!empty($projectId)) {

            $query->where("a.project_id = " . (int)$projectId);

        }



        $this->db->setQuery($query);

        $results = $this->db->loadAssocList();



        if (!$results) {

            $results = array();

        }



        $this->items = $results;

    }



    public function rewind()

    {

        $this->position = 0;

    }



    public function current()

    {

        return (!isset($this->items[$this->position])) ? null : $this->items[$this->position];

    }



    public function key()

    {

        return $this->position;

    }



    public function next()

    {

        ++$this->position;

    }



    public function valid()

    {

        return isset($this->items[$this->position]);

    }



    public function count()

    {

        return (int)count($this->items);

    }



    public function offsetSet($offset, $value)

    {

        if (is_null($offset)) {

            $this->items[] = $value;

        } else {

            $this->items[$offset] = $value;

        }

    }



    public function offsetExists($offset)

    {

        return isset($this->items[$offset]);

    }



    public function offsetUnset($offset)

    {

        unset($this->items[$offset]);

    }



    public function offsetGet($offset)

    {

        return isset($this->items[$offset]) ? $this->items[$offset] : null;

    }



    /**

     * Add a new value to the array.

     *

     * <code>

     * $partner = array(

     *     "name" => "John Dow",

     *     "project_id" => 1,

     *     "partner_id" => 2

     * );

     *

     * $partners   = new CrowdFundingPartnersPartners();

     * $partners->add($partner);

     * </code>

     *

     * @param array $value

     * @param null|int $index

     *

     * @return $this

     */

    public function add($value, $index = null)

    {

        if (!is_null($index)) {

            $this->items[$index] = $value;

        } else {

            $this->items[] = $value;

        }



        return $this;

    }

}

