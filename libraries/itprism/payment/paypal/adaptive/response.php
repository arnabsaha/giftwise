<?php
/**
 * @package      ITPrism
 * @subpackage   Payment\PayPal
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('JPATH_PLATFORM') or die;

/**
 * This class provides functionality for managing PayPal Adaptive Response.
 *
 * @package     ITPrism
 * @subpackage  Payment\PayPal
 */
class ITPrismPayPalAdaptiveResponse
{
    /**
     * An array that contains the response.
     *
     * @var array
     */
    protected $response;

    /**
     * Initialize the object.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * </code>
     *
     * @param array    $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * Return an array that contains response envelope data.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $envelope = $paypalResponse->getEnvelope();
     * </code>
     *
     * @return array
     */
    public function getEnvelope()
    {
        if (isset($this->response["responseEnvelope"])) {
            return $this->response["responseEnvelope"];
        }

        return array();
    }

    /**
     * Return a property value from the response envelope.
     * You can get following properties: timestamp, ack, correlationId, build;
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $ack = $paypalResponse->getEnvelopeProperty("ack");
     * </code>
     *
     * @param string $key
     *
     * @return null|mixed
     */
    public function getEnvelopeProperty($key)
    {
        if (isset($this->response["responseEnvelope"])) {
            return JArrayHelper::getValue($this->response["responseEnvelope"], $key);
        }

        return null;
    }

    /**
     * Return a payment key.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $payKey = $paypalResponse->getPayKey();
     * </code>
     *
     * @return mixed|null
     */
    public function getPayKey()
    {
        if (isset($this->response["payKey"])) {
            return JArrayHelper::getValue($this->response, "payKey");
        }

        return null;
    }

    /**
     * Return a payment execution status.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $paymentExecStatus = $paypalResponse->getPaymentExecStatus();
     * </code>
     *
     * @return mixed|null
     */
    public function getPaymentExecStatus()
    {
        if (isset($this->response["paymentExecStatus"])) {
            return JArrayHelper::getValue($this->response, "paymentExecStatus");
        }

        return null;
    }

    /**
     * Return a list with payment information.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $paymentInfoList = $paypalResponse->getPaymentInfoList();
     * </code>
     *
     * @return array
     */
    public function getPaymentInfoList()
    {
        if (isset($this->response["paymentInfoList"])) {
            return JArrayHelper::getValue($this->response, "paymentInfoList", array(), "array");
        }

        return array();
    }

    /**
     * Return a list with senders.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $senders = $paypalResponse->getSenders();
     * </code>
     *
     * @return array
     */
    public function getSenders()
    {
        if (isset($this->response["sender"])) {
            return JArrayHelper::getValue($this->response, "sender", array(), "array");
        }

        return array();
    }

    /**
     * Return a preapproval key.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * $preapprovalKey = $paypalResponse->getPreApprovalKey();
     * </code>
     *
     * @return string|null
     */
    public function getPreApprovalKey()
    {
        if (isset($this->response["preapprovalKey"])) {
            return JArrayHelper::getValue($this->response, "preapprovalKey");
        }

        return null;
    }

    /**
     * Check to see if the request has NOT been successfully process.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * if ($paypalResponse->isFailure()) {
     * ...
     * }
     * </code>
     *
     * @return bool
     */
    public function isFailure()
    {
        $ack = $this->getEnvelopeProperty("ack");

        if (strcmp($ack, "Failure") == 0) {
            return true;
        }

        return false;
    }

    /**
     * Check to see if the request has been successfully process.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * if ($paypalResponse->isSuccess()) {
     * ...
     * }
     * </code>
     *
     * @return bool
     */
    public function isSuccess()
    {
        $ack = $this->getEnvelopeProperty("ack");

        if (strcmp($ack, "Success") == 0) {
            return true;
        }

        return false;
    }

    /**
     * Generate and return an error message.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * if ($paypalResponse->isFailure()) {
     *     echo $paypalResponse->getErrorMessage();
     * }
     * </code>
     *
     * @return string
     */
    public function getErrorMessage()
    {
        $errors = JArrayHelper::getValue($this->response, "error", array(), "array");

        $errorMessage = array();

        if (isset($errors["errorId"])) {

            $errorCode = JArrayHelper::getValue($errors, "errorId");
            $errorParameters = JArrayHelper::getValue($errors, "parameter", array(), "array");

            $errorMessage[0]     = "[".JArrayHelper::getValue($errors, "domain") . ":" . JArrayHelper::getValue($errors, "subdomain") ."] [Error Code: ".$errorCode."]";
            $errorMessage[0]     .= "\n".JArrayHelper::getValue($errors, "message");
            $errorMessage[0]     .= "\nParameters: " .var_export($errorParameters, true);

        } elseif (isset($errors[0])) {

            foreach ($errors as $key => $error) {

                $errorCode = JArrayHelper::getValue($error, "errorId");
                $errorParameters = JArrayHelper::getValue($error, "parameter", array(), "array");

                $errorMessage[$key]     = "[".JArrayHelper::getValue($error, "domain") . ":" . JArrayHelper::getValue($error, "subdomain") ."] [Error Code: ".$errorCode."]";
                $errorMessage[$key]     .= "\n".JArrayHelper::getValue($error, "message");
                $errorMessage[$key]     .= "\nParameters: " .var_export($errorParameters, true);

            }

        }

        return implode("\n", $errorMessage);
    }

    /**
     * Return an error code.
     *
     * <code>
     * $response = array(...);
     *
     * $paypalResponse = new ITPrismPayPalAdaptiveResponse($response);
     * if ($paypalResponse->isFailure()) {
     *     echo $paypalResponse->getErrorCode();
     * }
     * </code>
     *
     * @return int
     */
    public function getErrorCode()
    {
        $errors = JArrayHelper::getValue($this->response, "error", array(), "array");

        $errorCode = 0;

        if (isset($errors["errorId"])) {
            $errorCode = JArrayHelper::getValue($errors, "errorId", 0, "int");
        } elseif (isset($errors[0])) {
            $errorCode = JArrayHelper::getValue($errors[0], "errorId", 0, "int");
        }

        return $errorCode;
    }
}
