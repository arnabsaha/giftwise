<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Libraries
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

if (!defined("CROWDFUNDINGFINANCE_PATH_COMPONENT_ADMINISTRATOR")) {
    define("CROWDFUNDINGFINANCE_PATH_COMPONENT_ADMINISTRATOR", JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingfinance");
}

if (!defined("CROWDFUNDINGFINANCE_PATH_COMPONENT_SITE")) {
    define("CROWDFUNDINGFINANCE_PATH_COMPONENT_SITE", JPATH_SITE . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingfinance");
}

if (!defined("CROWDFUNDINGFINANCE_PATH_LIBRARY")) {
    define("CROWDFUNDINGFINANCE_PATH_LIBRARY", JPATH_LIBRARIES . DIRECTORY_SEPARATOR . "crowdfundingfinance");
}

// Register version and constants
JLoader::register("CrowdFundingFinanceVersion", CROWDFUNDINGFINANCE_PATH_LIBRARY . DIRECTORY_SEPARATOR . "version.php");

// Register some helpers
JLoader::register(
    "CrowdFundingFinanceHelper",
    CROWDFUNDINGFINANCE_PATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . "helpers" . DIRECTORY_SEPARATOR . "crowdfundingfinance.php"
);
JLoader::register("CrowdFundingFinanceHelperRoute", CROWDFUNDINGFINANCE_PATH_COMPONENT_SITE . DIRECTORY_SEPARATOR . "helpers" . DIRECTORY_SEPARATOR . "route.php");

// Include HTML helpers path
JHtml::addIncludePath(CROWDFUNDINGFINANCE_PATH_COMPONENT_SITE . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'html');
