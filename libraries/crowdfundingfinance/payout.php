<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Payouts
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

/**
 * This class provides functionality that manage a payout.
 *
 * @package      CrowdFundingFinance
 * @subpackage   Payouts
 */
class CrowdFundingFinancePayout
{
    /**
     * Project ID.
     *
     * @var int
     */
    protected $id;

    protected $paypal_email;
    protected $paypal_first_name;
    protected $paypal_last_name;
    protected $iban;
    protected $bank_account;

    /**
     * Database driver.
     *
     * @var JDatabaseDriver
     */
    protected $db;

    /**
     * Initialize the object.
     *
     * <code>
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver  $db
     */
    public function __construct(JDatabaseDriver $db = null)
    {
        $this->db = $db;
    }

    /**
     * Set the database object.
     *
     * <code>
     * $payout    = new CrowdFundingFinancePayout();
     * $payout->setDb(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     *
     * @return self
     */
    public function setDb(JDatabaseDriver $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Load a payout data from database.
     *
     * <code>
     * $keys = array(
     *    "project_id" => 1
     * );
     *
     * $payout    = new CrowdFundingFinancePayout();
     * $payout->setDb(JFactory::getDbo());
     * $payout->load($keys);
     * </code>
     *
     * @param int $id Project ID
     */
    public function load($id)
    {
        $query = $this->db->getQuery(true);

        $query
            ->select(
                "a.id, a.paypal_email, a.paypal_first_name, a.paypal_last_name, a.iban, a.bank_account "
            )
            ->from($this->db->quoteName("#__cffinance_payouts", "a"))
            ->where("a.id = " . (int)$id);

        $this->db->setQuery($query);
        $result = $this->db->loadAssoc();

        if (!$result) {
            $result = array();
        }

        $this->bind($result);
    }

    /**
     * Set data to object properties.
     *
     * <code>
     * $data = array(
     *    "paypal_email"        => "todor@itprism.com",
     *    "paypal_first_name"   => "John",
     *    "paypal_last_name"    => "Dow"
     * );
     *
     * $payout    = new CrowdFundingFinancePayout();
     * $payout->bind($data);
     * </code>
     *
     * @param array $data
     * @param array $ignored
     */
    public function bind($data, $ignored = array())
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $ignored)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Return payout ID.
     *
     * <code>
     * $projectId  = 1;
     *
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * $payout->load($projectId);
     *
     * if (!$payout->getId()) {
     * ...
     * }
     * </code>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Return PayPal e-mail.
     *
     * <code>
     * $projectId  = 1;
     *
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * $payout->load($projectId);
     *
     * $paypalEmail = $payout->getPaypalEmail();
     * </code>
     */
    public function getPaypalEmail()
    {
        return $this->paypal_email;
    }

    /**
     * Return PayPal First Name.
     *
     * <code>
     * $projectId  = 1;
     *
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * $payout->load($projectId);
     *
     * $paypalFirstName = $payout->getPayPalFirstName();
     * </code>
     */
    public function getPaypalFirstName()
    {
        return (string)$this->paypal_first_name;
    }

    /**
     * Return PayPal last name.
     *
     * <code>
     * $projectId  = 1;
     *
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * $payout->load($projectId);
     *
     * $paypalLastName = $payout->getPayPalLastName();
     * </code>
     */
    public function getPaypalLastName()
    {
        return $this->paypal_last_name;
    }

    /**
     * Return the IBAN of the user where the amount should be sent.
     *
     * <code>
     * $projectId  = 1;
     *
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * $payout->load($projectId);
     *
     * $iban = $payout->getIban();
     * </code>
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Return information about user bank account.
     *
     * <code>
     * $projectId  = 1;
     *
     * $payout    = new CrowdFundingFinancePayout(JFactory::getDbo());
     * $payout->load($projectId);
     *
     * $bankAccount = $payout->getBankAccount();
     * </code>
     */
    public function getBankAccount()
    {
        return $this->bank_account;
    }

    /**
     * Returns an associative array of object properties.
     *
     * <code>
     * $keys = array(
     *  "user_id" => 1
     * );
     *
     * $payout    = new CrowdFundingFinancePayout();
     * $payout->setDb(JFactory::getDbo());
     * $payout->load($keys);
     *
     * $properties = $payout->getProperties();
     * </code>
     *
     * @return  array
     */
    public function getProperties()
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $value) {
            if (strcmp("db", $key) == 0) {
                unset($vars[$key]);
                break;
            }
        }

        return $vars;
    }
}
