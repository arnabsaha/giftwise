<?php
/**
 * @package      CrowdFundingFiles
 * @subpackage   Files
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_PLATFORM') or die;

/**
 * This class provides functionality that manage files.
 *
 * @package      CrowdFundingFiles
 * @subpackage   Files
 */
class CrowdFundingFilesFiles implements Iterator, Countable, ArrayAccess
{
    protected $items = array();

    protected $position = 0;

    /**
     * Database driver.
     *
     * @var JDatabaseDriver
     */
    protected $db;

    /**
     * Initialize the object.
     *
     * <code>
     * $files   = new CrowdFundingFilesFiles(JFactory::getDbo());
     * </code>
     *
     * @param JDatabaseDriver $db
     */
    public function __construct(JDatabaseDriver $db)
    {
        $this->db = $db;
    }

    /**
     * Load files data by ID from database.
     *
     * <code>
     * $ids = array(1,2,3,4,5);
     *
     * $files   = new CrowdFundingFilesFiles(JFactory::getDbo());
     * $files->load($ids);
     *
     * foreach($files as $file) {
     *   echo $file["title"];
     *   echo $file["filename"];
     * }
     *
     * </code>
     *
     * @param int $projectId
     * @param int $userId
     * @param array $ids
     */
    public function load($projectId = 0, $userId = 0, $ids = array())
    {
        // Load project data
        $query = $this->db->getQuery(true);

        $query
            ->select("a.id, a.title, a.filename, a.project_id, a.user_id")
            ->from($this->db->quoteName("#__cffiles_files", "a"));

        if (!empty($ids)) {
            JArrayHelper::toInteger($ids);
            $query->where("a.id IN ( " . implode(",", $ids) . " )");
        }

        if (!empty($projectId)) {
            $query->where("a.project_id = " . (int)$projectId);
        }

        if (!empty($userId)) {
            $query->where("a.user_id = " . (int)$userId);
        }

        $this->db->setQuery($query);
        $results = $this->db->loadAssocList();

        if (!$results) {
            $results = array();
        }

        $this->items = $results;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return (!isset($this->items[$this->position])) ? null : $this->items[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    public function count()
    {
        return (int)count($this->items);
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->items[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->items[$offset]) ? $this->items[$offset] : null;
    }

}
