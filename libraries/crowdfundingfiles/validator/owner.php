<?php
/**
 * @package      CrowdFundingFiles
 * @subpackage   Validators
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('JPATH_BASE') or die;

JLoader::register("ITPrismFileValidator", JPATH_LIBRARIES . "/itprism/validator/interface.php");

/**
 * This class provides functionality validation file owner.
 *
 * @package      CrowdFundingFiles
 * @subpackage   Validators
 */
class CrowdFundingFilesValidatorOwner implements ITPrismValidatorInterface
{
    protected $db;
    protected $fileId;
    protected $userId;

    /**
     * Initialize the object.
     *
     * <code>
     * $fileId = 1;
     * $userId = 2;
     *
     * $file   = new CrowdFundingFilesValidatorOwner(JFactory::getDbo(), $fileId, $userId);
     * </code>
     *
     * @param JDatabaseDriver $db Database object.
     * @param int $fileId File ID.
     * @param int $userId User ID.
     */
    public function __construct($db, $fileId, $userId)
    {
        $this->db      = $db;
        $this->fileId = $fileId;
        $this->userId  = $userId;
    }

    /**
     * Validate image owner.
     *
     * <code>
     * $fileId = 1;
     * $userId = 2;
     *
     * $file   = new CrowdFundingFilesValidatorOwner(JFactory::getDbo(), $fileId, $userId);
     * if (!$file->isValid()) {
     * ...
     * }
     * </code>
     *
     * @throws RuntimeException
     */
    public function isValid()
    {
        $query = $this->db->getQuery(true);
        $query
            ->select("COUNT(*)")
            ->from($this->db->quoteName("#__cffiles_files", "a"))
            ->where("a.id = " .(int)$this->fileId)
            ->where("a.user_id = " . (int)$this->userId);

        $this->db->setQuery($query, 0, 1);
        $result = $this->db->loadResult();

        return (bool)$result;
    }
}
