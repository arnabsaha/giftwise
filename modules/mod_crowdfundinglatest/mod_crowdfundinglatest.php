<?php
/**
 * @package      CrowdFunding
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined("_JEXEC") or die;

$moduleclassSfx = htmlspecialchars($params->get('moduleclass_sfx'));

jimport("itprism.init");
jimport("crowdfunding.init");
JLoader::register("CrowdFundingLatestModuleHelper", JPATH_ROOT . "/modules/mod_crowdfundinglatest/helper.php");

$limitResults = $params->get("results_limit", 5);
if ($limitResults <= 0) {
    $limitResults = 5;
}

$projects = CrowdFundingLatestModuleHelper::getProjects($limitResults);

// Get options
$displayInfo        = $params->get("display_info", 1);
$displayDescription = $params->get("display_description", 0);
$titleLength        = $params->get("title_length", 32);
$descriptionLength  = $params->get("description_length", 63);
$displayCreator     = $params->get("display_creator", true);

// Get component parameters
/** @var  $componentParams Joomla\Registry\Registry */
$componentParams = JComponentHelper::getParams("com_crowdfunding");
$imagesDirectory = $componentParams->get("images_directory", "images/crowdfunding");

if ($displayInfo) {
    // Get currency
    jimport("crowdfunding.currency");
    $currencyId = $componentParams->get("project_currency");
    $currency   = CrowdFundingCurrency::getInstance(JFactory::getDbo(), $currencyId, $componentParams);
}

// Display user social profile ( integrate ).
if (!empty($displayCreator)) {
    $socialProfiles = null;

    // Get users IDs
    $usersIds = array();
    foreach ($projects as $project) {
        $usersIds[] = $project->user_id;
    }

    // Get a social platform for integration
    $socialPlatform = $componentParams->get("integration_social_platform");

    // Load the class
    if (!empty($socialPlatform)) {
        jimport("itprism.integrate.profiles");
        $socialProfiles = ITPrismIntegrateProfiles::factory($socialPlatform, $usersIds);
    }
}

if (!empty($projects)) {
    require JModuleHelper::getLayoutPath('mod_crowdfundinglatest', $params->get('layout', 'default'));
}
