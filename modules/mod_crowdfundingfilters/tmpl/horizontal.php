<?php
/**
 * @package      CrowdFunding
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
 
// no direct access
defined('_JEXEC') or die; ?>
<div class="cf-modfilters<?php echo $moduleclassSfx; ?>">
    <form action="<?php echo JRoute::_(CrowdFundingHelperRoute::getDiscoverRoute());?>" method="get">
        <div class="row">
            <?php if(!empty($displayCategories)) {?>
            <div class="span4">
                <select name="filter_category" class="js-modfilters-filter">
                    <?php echo JHtml::_("select.options", $categories, "value", "text", $filterCategory);?>
                </select>
            </div>
            <?php }?>
        
            <?php if(!empty($displayCountries)) {?>
            <div class="span4">
                <select name="filter_country" class="js-modfilters-filter">
                    <?php echo JHtml::_("select.options", $countries, "value", "text", $filterCountry);?>
                </select>
            </div>
            <?php }?>
        
            <?php if(!empty($displayFundingTypes)) {?>
            <div class="span4">
                <select name="filter_fundingtype" class="js-modfilters-filter">
                    <?php echo JHtml::_("select.options", $fundingTypes, "value", "text", $filterFundingType);?>
                </select>
            </div>
            <?php }?>
            
            <?php if(!empty($displayProjectTypes)) {?>
            <div class="span4">
                <select name="filter_projecttype" class="js-modfilters-filter">
                    <?php echo JHtml::_("select.options", $projectTypes, "value", "text", $filterProjectType);?>
                </select>
            </div>
            <?php }?>
            
        </div>
        
        <div class="clearfix"></div>
        <button type="submit" class="btn btn-primary"><?php echo JText::_("MOD_CROWDFUNDINGFILTERS_SUBMIT");?></button>
    </form>
</div>