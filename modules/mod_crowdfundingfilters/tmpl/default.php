<?php
/**
 * @package      CrowdFunding
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die; ?>
<div class="cf-modfilters<?php echo $moduleclassSfx; ?>">
    <form action="<?php echo JRoute::_(CrowdFundingHelperRoute::getDiscoverRoute());?>" method="get">
        <?php if(!empty($displayCategories)) {?>
        <select name="filter_category" class="js-modfilters-filter">
            <?php echo JHtml::_("select.options", $categories, "value", "text", $filterCategory);?>
        </select>
        <div class="clearfix"></div>
        <?php }?>

        <?php if(!empty($displayCountries)) {?>
        <select name="filter_country" class="js-modfilters-filter">
            <?php echo JHtml::_("select.options", $countries, "value", "text", $filterCountry);?>
        </select>
        <div class="clearfix"></div>
        <?php }?>

        <?php if(!empty($displayFundingTypes)) {?>
        <select name="filter_fundingtype" class="js-modfilters-filter">
            <?php echo JHtml::_("select.options", $fundingTypes, "value", "text", $filterFundingType);?>
        </select>
        <div class="clearfix"></div>
        <?php }?>

        <?php if(!empty($displayProjectTypes)) {?>
        <select name="filter_projecttype" class="js-modfilters-filter">
            <?php echo JHtml::_("select.options", $projectTypes, "value", "text", $filterProjectType);?>
        </select>
        <div class="clearfix"></div>
        <?php }?>

        <button type="submit" class="btn btn-primary"><?php echo JText::_("MOD_CROWDFUNDINGFILTERS_SUBMIT");?></button>
    </form>
</div>