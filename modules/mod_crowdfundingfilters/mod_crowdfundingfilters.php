<?php
/**
 * @package      CrowdFunding
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined("_JEXEC") or die;

jimport("itprism.init");
jimport("crowdfunding.init");

$moduleclassSfx = htmlspecialchars($params->get('moduleclass_sfx'));

if ($params->get("enable_chosen", 0)) {
    JHtml::_('formbehavior.chosen', 'select.js-modfilters-filter');
}

$app = JFactory::getApplication();
/** @var $app JApplicationSite */

$filterPhrase = $app->getUserStateFromRequest("mod_crowdfundingfilters.filter_phrase", "filter_phrase", "");

// Get options
$displayCountries    = $params->get("display_countries", 0);
$displayCategories   = $params->get("display_categories", 0);
$displayFundingTypes = $params->get("display_funding_type", 0);
$displayProjectTypes = $params->get("display_project_type", 0);

if (!empty($displayCountries)) {
    $filterCountry = $app->getUserStateFromRequest("mod_crowdfundingfilters.filter_country", "filter_country");

    jimport("crowdfunding.filters");
    $filters   = CrowdFundingFilters::getInstance(JFactory::getDbo());
    $countries = $filters->getCountries();

    $option = JHtml::_("select.option", "", JText::_("MOD_CROWDFUNDINGFILTERS_SELECT_COUNTRY"), "value", "text");
    $option = array($option);

    $countries = array_merge($option, $countries);
}

if (!empty($displayCategories)) {
    $filterCategory = $app->getUserStateFromRequest("mod_crowdfundingfilters.filter_category", "filter_category");

    $config     = array(
        "filter.published" => 1
    );
    $categories = JHtml::_("category.options", "com_crowdfunding", $config);

    $option = JHtml::_("select.option", 0, JText::_("MOD_CROWDFUNDINGFILTERS_SELECT_CATEGORY"));
    $option = array($option);

    $categories = array_merge($option, $categories);
}

if (!empty($displayFundingTypes)) {
    $filterFundingType = $app->getUserStateFromRequest("mod_crowdfundingfilters.filter_fundingtype", "filter_fundingtype");

    $fundingTypes = array(
        JHtml::_("select.option", "", JText::_("MOD_CROWDFUNDINGFILTERS_SELECT_FUNDING_TYPE")),
        JHtml::_("select.option", "fixed", JText::_("MOD_CROWDFUNDINGFILTERS_FIXED")),
        JHtml::_("select.option", "flexible", JText::_("MOD_CROWDFUNDINGFILTERS_FLEXIBLE"))
    );
}

if (!empty($displayProjectTypes)) {
    $filterProjectType = $app->getUserStateFromRequest("mod_crowdfundingfilters.filter_projecttype", "filter_projecttype");

    jimport("crowdfunding.filters");
    $filters      = CrowdFundingFilters::getInstance(JFactory::getDbo());
    $projectTypes = $filters->getProjectsTypes();

    $optionSelect = array(0 =>
                              array(
                                  "value" => 0,
                                  "text"  => JText::_("MOD_CROWDFUNDINGFILTERS_SELECT_PROJECT_TYPE")
                              )
    );
    $projectTypes = array_merge($optionSelect, $projectTypes);
}

require JModuleHelper::getLayoutPath('mod_crowdfundingfilters', $params->get('layout', 'default'));