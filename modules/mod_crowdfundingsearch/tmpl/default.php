<?php
/**
 * @package      CrowdFunding
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die; ?>
<div class="cf-modsearch<?php echo $moduleclassSfx; ?>">
    <form action="<?php echo JRoute::_(CrowdFundingHelperRoute::getDiscoverRoute()); ?>" method="get">
        <input type="text" name="filter_phrase" value="<?php echo $filterPhrase; ?>"
               placeholder="<?php echo JText::_("MOD_CROWDFUNDINGSEARCH_SEARCH_FOR"); ?>"/>

        <div class="clearfix"></div>

        <?php if (!empty($displayCategories)) { ?>
            <select name="filter_category" class="js-modsearch-filter">
                <?php echo JHtml::_("select.options", $categories, "value", "text", $filterCategory); ?>
            </select>
            <div class="clearfix"></div>
        <?php } ?>

        <?php if (!empty($displayCountries)) { ?>
            <select name="filter_country" class="js-modsearch-filter">
                <?php echo JHtml::_("select.options", $countries, "value", "text", $filterCountry); ?>
            </select>
            <div class="clearfix"></div>
        <?php } ?>

        <?php if (!empty($displayProjectTypes)) { ?>
            <select name="filter_projecttype" class="js-modsearch-filter">
                <?php echo JHtml::_("select.options", $projectTypes, "value", "text", $filterProjectType); ?>
            </select>
            <div class="clearfix"></div>
        <?php } ?>

        <button type="submit" class="btn btn-primary"><?php echo JText::_("MOD_CROWDFUNDINGSEARCH_SEARCH"); ?></button>
    </form>
</div>