<?php
/**
 * @package      CrowdFunding
 * @subpackage   Modules
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die; ?>
<div class="cf-modsearch<?php echo $moduleclassSfx; ?>">
    <form action="<?php echo JRoute::_(CrowdFundingHelperRoute::getDiscoverRoute()); ?>" method="get">
        <div class="row-fluid">
            <div class="span12">
                <input type="text" name="filter_phrase" value="<?php echo $filterPhrase; ?>"
                       placeholder="<?php echo JText::_("MOD_CROWDFUNDINGSEARCH_SEARCH_FOR"); ?>" class="span12"/>
            </div>
        </div>

        <div class="row">
            <?php if (!empty($displayCategories)) { ?>
                <div class="span4">
                    <select name="filter_category" class="js-modsearch-filter">
                        <?php echo JHtml::_("select.options", $categories, "value", "text", $filterCategory); ?>
                    </select>
                </div>
            <?php } ?>

            <?php if (!empty($displayCountries)) { ?>
                <div class="span4">
                    <select name="filter_country" class="js-modsearch-filter">
                        <?php echo JHtml::_("select.options", $countries, "value", "text", $filterCountry); ?>
                    </select>
                </div>
            <?php } ?>

            <?php if (!empty($displayProjectTypes)) { ?>
                <div class="span4">
                    <select name="filter_projecttype" class="js-modsearch-filter">
                        <?php echo JHtml::_("select.options", $projectTypes, "value", "text", $filterProjectType); ?>
                    </select>
                </div>
            <?php } ?>

        </div>

        <div class="clearfix"></div>
        <button type="submit" class="btn btn-primary"><?php echo JText::_("MOD_CROWDFUNDINGSEARCH_SEARCH"); ?></button>
    </form>
</div>