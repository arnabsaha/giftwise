<?php
/**
 * @package      CrowdFundingPartners
 * @subpackage   Plugins
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2015 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('_JEXEC') or die;

// Load the script that initializes the select element with banks.
$doc->addScript("plugins/crowdfunding/partners/js/script.js?v=" . rawurlencode($this->version));
?>

<div class="row-fluid">
    <div class="span12 well">

        <h3><?php echo JText::_("Add Product Details");?></h3>

        <form action="<?php echo JRoute::_("index.php?option=com_crowdfundingpartners&task=partners.addPartner&format=raw"); ?>" method="post" class="form-search" id="js-cfpartners-form" autocomplete="off">
            <input type="text" name="prodname" placeholder="<?php echo "Enter Product Name";// JText::_("PLG_CROWDFUNDING_PARTNERS_ENTER_USERNAME");?>" required></br></br>
	    <input type="url" name="url" placeholder="<?php echo "Enter Product URL";?>" required></br></br>
	    <input type="number" name="price" placeholder="<?php echo "Product Price";?>">
	    <!--input type="file" name="fileToUpload" id="fileToUpload"-->

            <button class="btn bnt-primary" type="submit">
                <i class="icon-plus-sign"></i>
                <?php echo JText::_("PLG_CROWDFUNDING_PARTNERS_ADD_PARTNER");?>
            </button>
            <img src="/media/com_crowdfunding/images/ajax-loader.gif" width="16" height="16" id="js-cfpartners-ajax-loader" class="hide" />

            <input type="hidden" name="project_id" value="<?php echo (int)$item->id; ?>"/>
        </form>

        <table class="table table-bordered mtb_25_0">
            <thead>
            <tr>
                <th class="span1">&nbsp;</th>
                <th class="span9"><?php echo JText::_("Product Name");?></th>
                <th class="span9"><?php echo JText::_("Product URL");?></th>
                <th class="span5"><?php echo JText::_("Price");?></th>
                <th class="span2">&nbsp;</th>
            </tr>
            </thead>
            <tbody id="js-cfpartners-list">
            <?php foreach ($partners as $partner) { ?>
                <tr id="js-cfpartners-partner<?php echo $partner["id"]; ?>">
                    <td>
                        <img src="<?php echo $partner["avatar"]; ?>" />
                    </td>
                    <td>
                        <?php echo htmlentities($partner["prodname"], ENT_QUOTES, "UTF-8"); ?>
                    </td>
		            <td>
                        <?php echo htmlentities($partner["produrl"], ENT_QUOTES, "UTF-8"); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($partner["price"], ENT_QUOTES, "UTF-8"); ?>
                    </td>
                    <td>
                        <a class="btn btn-danger js-cfpartners-btn-remove" href="<?php echo JRoute::_("index.php?option=com_crowdfundingpartners&task=partners.remove&format=raw");?>" data-partner-id="<?php echo (int)$partner["id"]; ?>">
                            <i class="icon-trash"></i>
                            <span class="hidden-phone"><?php echo JText::_("PLG_CROWDFUNDING_PARTNERS_REMOVE");?></span>
                        </a>
                    </td>
                </tr>
            <?php } ?>

            <tr style="display: none;" id="js-cfpartners-element">
                <td>
                    <img src="" />
                </td>
                <td>{NAME}</td>
                <td>
                    <a class="btn btn-danger" href="<?php echo JRoute::_("index.php?option=com_crowdfundingpartners&task=partners.remove&format=raw");?>">
                        <i class="icon-trash"></i>
                        <span class="hidden-phone"><?php echo JText::_("PLG_CROWDFUNDING_PARTNERS_REMOVE");?></span>
                    </a>
                </td>
            </tr>

            </tbody>
        </table>

    </div>

</div>

