<?php
/**
 * @package         CrowdFundingFiles
 * @subpackage      Plugins
 * @author          Todor Iliev
 * @copyright       Copyright (C) 2015 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

jimport("crowdfunding.init");
jimport("crowdfundingfiles.init");

/**
 * CrowdFunding Files Plugin
 *
 * @package        CrowdFundingFiles
 * @subpackage     Plugins
 */
class plgCrowdFundingFiles extends JPlugin
{
    protected $autoloadLanguage = true;

    /**
     * @var JApplicationSite
     */
    protected $app;

    /**
     * @var Joomla\Registry\Registry
     */
    public $params;

    protected $version = "1.0";

    /**
     * This method prepares a code that will be included to step "Extras" on project wizard.
     *
     * @param string    $context This string gives information about that where it has been executed the trigger.
     * @param object    $item    A project data.
     * @param Joomla\Registry\Registry $params  The parameters of the component
     *
     * @return null|string
     */
    public function onExtrasDisplay($context, &$item, &$params)
    {
        if (strcmp("com_crowdfunding.project.extras", $context) != 0) {
            return null;
        }

        if ($this->app->isAdmin()) {
            return null;
        }

        $doc = JFactory::getDocument();
        /**  @var $doc JDocumentHtml */

        // Check document type
        $docType = $doc->getType();
        if (strcmp("html", $docType) != 0) {
            return null;
        }
        
        if (empty($item->user_id)) {
            return null;
        }

        // Create a media folder.
        $mediaFolder = CrowdFundingFilesHelper::getMediaFolder();
        if (!JFolder::exists($mediaFolder)) {
            CrowdFundingHelper::createFolder($mediaFolder);
        }

        // Create a media folder for a user.
        $mediaFolder = CrowdFundingFilesHelper::getMediaFolder($item->user_id);
        if (!JFolder::exists($mediaFolder)) {
            CrowdFundingHelper::createFolder($mediaFolder);
        }

        $componentParams = JComponentHelper::getParams("com_crowdfundingfiles");
        /** @var  $componentParams Joomla\Registry\Registry */

        $mediaUri = CrowdFundingFilesHelper::getMediaFolderUri($item->user_id);

        jimport("crowdfundingfiles.files");
        $files = new CrowdFundingFilesFiles(JFactory::getDbo());
        $files->load($item->id, $item->user_id);

        // Load jQuery
        JHtml::_("jquery.framework");
        JHtml::_("itprism.ui.pnotify");
        JHtml::_('itprism.ui.fileupload');
        JHtml::_('itprism.ui.joomla_helper');

        // Include the translation of the confirmation question.
        JText::script('PLG_CROWDFUNDING_FILES_DELETE_QUESTION');

        // Get the path for the layout file
        $path = JPath::clean(JPluginHelper::getLayoutPath('crowdfunding', 'files'));

        // Render the login form.
        ob_start();
        include $path;
        $html = ob_get_clean();

        return $html;
    }
}
