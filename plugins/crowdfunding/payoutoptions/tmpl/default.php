<?php
/**
 * @package      CrowdFundingPayoutOptions
 * @subpackage   Plugins
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('_JEXEC') or die;

// Load the script that initializes the select element with banks.
$doc->addScript("plugins/crowdfunding/payoutoptions/js/script.js?v=" . rawurlencode($this->version));
?>

<div class="row-fluid">
    <div class="span12 well">

        <h3><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_PAYOUT_OPTIONS");?></h3>

        <form action="<?php echo JRoute::_("index.php?option=com_crowdfundingfinance"); ?>" method="post" id="js-cfpayoutoptions-form" autocomplete="off">

            <?php echo JHtml::_('bootstrap.startTabSet', 'cfPayoutOptions', array('active' => $activeTab)); ?>

            <?php if($this->params->get("display_paypal", 0)) { ?>
            <?php echo JHtml::_('bootstrap.addTab', 'cfPayoutOptions', 'paypal', JText::_('PLG_CROWDFUNDING_PAYOUTOPTIONS_PAYPAL')); ?>
            <div class="row-fluid">
                <div class="span8">
                    <div class="control-group">
                        <label for="cf-payoutoptions-paypal-first-name" class="control-label"><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_PAYPAL_FIRST_NAME");?></label>
                        <div class="controls">
                            <input type="text" name="paypal_first_name" id="cf-payoutoptions-paypal-first-name" value="<?php echo $payout->getPaypalFirstName(); ?>" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cf-payoutoptions-paypal-last-name" class="control-label"><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_PAYPAL_LAST_NAME");?></label>
                        <div class="controls">
                            <input type="text" name="paypal_last_name" id="cf-payoutoptions-paypal-last-name" value="<?php echo $payout->getPaypalLastName(); ?>" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cf-payoutoptions-paypal-email" class="control-label"><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_PAYPAL_EMAIL");?></label>
                        <div class="controls">
                            <input type="text" name="paypal_email" id="cf-payoutoptions-paypal-email" value="<?php echo $payout->getPaypalEmail(); ?>" class="input-xlarge">
                        </div>
                    </div>

                </div>
                <?php if($this->params->get("display_paypal_info", 1)) { ?>
                <div class="span4 cf-information-box">
                    <h4><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_PAYPAL_INFORMATION");?></h4>

                    <p><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_NOTE_PAYPAL_ACCOUNT");?></p>

                    <?php if(!$this->params->get("paypal_requirements_link")) { ?>
                    <p><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_NOTE_PAYPAL_REQUIREMENTS");?></p>
                    <?php } else { ?>
                    <p><?php echo JText::sprintf("PLG_CROWDFUNDING_PAYOUTOPTIONS_NOTE_PAYPAL_REQUIREMENTS_S", $this->params->get("paypal_requirements_link"));?></p>
                    <?php } ?>

                    <?php if($this->params->get("paypal_additional_information")) { ?>
                        <p><?php echo htmlentities($this->params->get("paypal_additional_information"), ENT_QUOTES, "UTF-8");?></p>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php } ?>

            <?php if($this->params->get("display_banktransfer", 0)) { ?>
                <?php echo JHtml::_('bootstrap.addTab', 'cfPayoutOptions', 'banktransfer', JText::_('PLG_CROWDFUNDING_PAYOUTOPTIONS_BANKTRANSFER')); ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="control-group">
                            <label for="cf-payoutoptions-banktransfer-iban" class="control-label"><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_IBAN");?></label>
                            <div class="controls">
                                <input type="text" name="iban" id="cf-payoutoptions-banktransfer-iban" class="input-xxlarge" value="<?php echo $payout->getIban(); ?>">
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="cf-payoutoptions-banktransfer-bank_account" class="control-label"><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_ADDITIONAL_INFORMATION");?></label>
                            <div class="controls">
                                <textarea name="bank_account" id="cf-payoutoptions-banktransfer-bank_account" class="input-xxlarge" rows="6"><?php echo $payout->getBankAccount(); ?></textarea>
                            </div>
                        </div>

                    </div>
                    <?php if($this->params->get("display_banktransfer_info", 1)) { ?>
                        <div class="span4 cf-information-box">
                            <h4><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_BANKTRANSFER_INFORMATION");?></h4>

                            <p><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_NOTE_BANKTRANSFER_ACCOUNT");?></p>

                            <?php if(!$this->params->get("banktransfer_requirements_link")) { ?>
                                <p><?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_NOTE_BANKTRANSFER_REQUIREMENTS");?></p>
                            <?php } else { ?>
                                <p><?php echo JText::sprintf("PLG_CROWDFUNDING_PAYOUTOPTIONS_NOTE_BANKTRANSFER_REQUIREMENTS_S", $this->params->get("banktransfer_requirements_link"));?></p>
                            <?php } ?>

                            <?php if($this->params->get("banktransfer_additional_information")) { ?>
                                <p><?php echo htmlentities($this->params->get("banktransfer_additional_information"), ENT_QUOTES, "UTF-8");?></p>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php } ?>

            <?php echo JHtml::_('bootstrap.endTabSet'); ?>

            <div class="control-group">
                <div class="controls">
                    <button class="btn bnt-primary" type="submit">
                        <i class="icon-save"></i>
                        <?php echo JText::_("PLG_CROWDFUNDING_PAYOUTOPTIONS_SAVE");?>
                    </button>
                    <img src="/media/com_crowdfunding/images/ajax-loader.gif" width="16" height="16" id="js-cfpayoutoptions-ajax-loader" class="hide" />
                </div>
            </div>

            <input type="hidden" name="task" value="payout.save"/>
            <input type="hidden" name="format" value="raw"/>
            <input type="hidden" name="project_id" value="<?php echo (int)$item->id; ?>"/>
        </form>

    </div>

</div>

