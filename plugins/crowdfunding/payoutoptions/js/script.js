jQuery(document).ready(function() {

    // Initialize the form
    jQuery("#js-cfpayoutoptions-form").on("submit", function(event) {
        event.preventDefault();

        var url = jQuery(this).attr("action");
        var fields = jQuery(this).serialize();

        jQuery.ajax({
            url: url,
            type: "POST",
            data: fields,
            dataType: "text json",
            beforeSend: function() {
                jQuery("#js-cfpayoutoptions-ajax-loader").show();
            }
        }).done(function(response) {

                if(!response.success) {
                    ITPrismUIHelper.displayMessageFailure(response.title, response.text);
                } else {
                    ITPrismUIHelper.displayMessageSuccess(response.title, response.text);
                }

                // Hide ajax loader.
                jQuery("#js-cfpayoutoptions-ajax-loader").hide();

            }
        );

    });

});