<?php
defined ('_JEXEC') or die();

/**
 * @author Valérie Isaksen
 * @version $Id$
 * @package VirtueMart
 * @subpackage payment
 * @copyright Copyright (C) 2004-Copyright (C) 2004-2014 Virtuemart Team. All rights reserved.   - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
 *
 * http://virtuemart.net
 */

?>
<div class="post_payment_payment_name" style="width: 100%">
	<span class=post_payment_payment_name_title"><?php echo vmText::_ ('VMPAYMENT_STANDARD_PAYMENT_INFO'); ?> </span>
	<?php echo  $viewData["payment_name"]; ?>
</div>

<div class="post_payment_order_number" style="width: 100%">
	<span class=post_payment_order_number_title"><?php echo vmText::_ ('COM_VIRTUEMART_ORDER_NUMBER'); ?> </span>
	<?php echo  $viewData["order_number"]; ?>
</div>

<div class="post_payment_order_total" style="width: 100%">
	<span class="post_payment_order_total_title"><?php echo vmText::_ ('COM_VIRTUEMART_ORDER_PRINT_TOTAL'); ?> </span>
	<?php echo  $viewData['displayTotalInPaymentCurrency']; 

			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "giftwise";

			// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
			// Check connection
			if ($conn->connect_error) {
  			  die("Connection failed: " . $conn->connect_error);
			} 

	
			define( '_JEXEC', 1 );
			define('JPATH_BASE', '..');//this is when we are in the root
			define( 'DS', DIRECTORY_SEPARATOR );
	 
			require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
			require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	 
			$mainframe =& JFactory::getApplication('site');
			$mainframe->initialise();

    		$session =& JFactory::getSession();
   
			$my =& JFactory::getUser();
			$joomla_name = $my->name;
			$joomla_id = $my->id;
	
			if($joomla_id == 0)
			{
				header('Location: /Giftwise/index.php/user-login');
			}
			else
			{
				if(isset($_SESSION['oid']))
				{
			$sql = "INSERT INTO nkgja_aup_contributor_details(virtuemart_user_id, projectowner_user_id, amount, status, orderid)
			VALUES ('".$joomla_id."','".$_SESSION['oid']."','".$_SESSION['quantity']."', 'pending','".$viewData["order_number"]."')";

				if ($conn->query($sql) === TRUE) {
				    echo "</br>Contributor details added";
				    unset($_SESSION['oid']);
				    unset($_SESSION['quantity']);
				} else {
	   				 echo "Error: " . $sql . "<br>" . $conn->error;
				}
				}
			}

			$conn->close();
?>
</div>
<a class="vm-button-correct" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$viewData["order_number"].'&order_pass='.$viewData["order_pass"], false)?>"><?php echo vmText::_('COM_VIRTUEMART_ORDER_VIEW_ORDER'); ?></a>






