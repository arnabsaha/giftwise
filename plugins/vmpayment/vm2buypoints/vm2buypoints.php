<?php
defined('_JEXEC') or 	die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;
/**
 * @version $Id: vm2buypoints.php
 *
 * @author Arnab Saha
 * @package VirtueMart
 * @subpackage payment
 * @copyright Copyright (C) 2004-2010 Nordmograph All rights reserved.
 */
if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
if (!class_exists('vmCustomPlugin')) require(JPATH_VM_PLUGINS . DS . 'vmcustomplugin.php');
class plgVmPaymentVm2buypoints extends vmCustomPlugin {
	public function plgVmOnUpdateOrderPayment(  $_formData) {
		$db = &JFactory::getDBO();
		$app 			= JFactory::getApplication();
		$ratio			= $this->params->def('ratio', '100');
		$pointsystem 	= $this->params->def('pointsystem', 1);
		$vmcatid 		= $this->params->def('vmcatid', '1');
		$virtuemart_order_id = $_formData->virtuemart_order_id;
		
	 	if ($_formData->order_status == "C") {
					//$order_number 	= $order['details']['BT']->order_number;		
			$q ="SELECT `virtuemart_user_id` FROM `#__virtuemart_orders` WHERE `virtuemart_order_id` ='".$virtuemart_order_id."'  " ;		
			$db->setQuery($q);
			$virtuemart_user_id  = $db->loadResult();

			//echo "Virtuemart Order ID: ".$virtuemart_order_id;

			$q = "SELECT `order_number` FROM `#__virtuemart_orders` WHERE `virtuemart_order_id` ='".$virtuemart_order_id."'";
			$db->setQuery($q);
			

			$order_number = $db->loadresult();

			//update contributor details 
			$q = "SELECT projectowner_user_id FROM `#__aup_contributor_details` WHERE `orderid` ='".$order_number."'";
			$db->setQuery($q);

				if($db->loadResult())
				{
					$virtuemart_user_id = $db->loadResult();

					$q ="UPDATE `#__aup_contributor_details` SET `status` = 'success' WHERE `orderid`='".$order_number."'"  ;
					$db->setQuery($q);
					if (!$db->query()) die($db->stderr(true));
				}
				
			
			/*$q = "SELECT `projectowner_user_id` FROM `#__aup_contributor_details` WHERE `orderid` ='".$order_number."'  ";
			$db->setQuery($q);
			$owneritems = $db->loadObjectList();
			
			
			foreach ($owneritems as $owner){
				$projectowner_user_id = $owner->projectowner_user_id;*/
			
			
			$q ="SELECT voi.`virtuemart_order_item_id` , voi.`virtuemart_product_id` ,voi.`order_item_sku` , voi.`order_item_name` , voi.`product_quantity` , voi.`product_item_price` ,
					vpc.`virtuemart_category_id` 
					FROM `#__virtuemart_order_items` voi 
					LEFT JOIN `#__virtuemart_product_categories` vpc ON vpc.`virtuemart_product_id` = voi.`virtuemart_product_id` 
					WHERE `virtuemart_order_id` = '".$virtuemart_order_id."' ";
			$db->setQuery($q);
			$items = $db->loadObjectList();
			foreach ($items as $item){
				$virtuemart_order_item_id	= $item->virtuemart_order_item_id;
				$virtuemart_category_id 	= $item->virtuemart_category_id; 
				$product_item_price 		= $item->product_item_price;
				$product_quantity 			= $item->product_quantity;
				$order_item_sku				= $item->order_item_sku;		
				if( $virtuemart_category_id == $vmcatid ){

				
		
				
				
				
				
				
				/*$q ="DELETE FROM `#__aup_contributor_details` WHERE `virtuemart_user_id`='".$contributor_user_id."'AND `projectowner_user_id`='".$virtuemart_user_id."'AND `amount`='".$product_item_price."' AND `orderid` != 'pending'"  ;
				$db->setQuery($q);*/	
				
					$pts2credit = $product_item_price * $product_quantity * $ratio;
					if($pointsystem ==1){  // AUP
						// check if AUP rule is installed 
						$q ="SELECT COUNT(*) FROM #__alpha_userpoints_rules WHERE plugin_function ='plgaup_vm2buypoints_purchase' ";
						$db->setQuery($q);
						$is_installed = $db->loadResult();
						if($is_installed<1){
							$q = "INSERT INTO `#__alpha_userpoints_rules` ( `rule_name` , `rule_description` , `rule_plugin` , `plugin_function` ,  `access` , `published` , `system` , `autoapproved` , `fixedpoints` , `category` , `displaymsg` , `method`  )
							VALUES ( '".JText::_( 'PLG_VM2BUYPOINTS_AUPPURCHASERULENAME' )."' , '".JText::_( 'PLG_VM2BUYPOINTS_AUPPURCHASERULENAME_DESC' )."' , 'AUP_VM2BUYPOINTS' , 'plgaup_vm2buypoints_purchase' , '1' , '1' , '0' , '1' , '0' , 'pu' , '0' , '4' )";
							$db->setQuery($q);
							if (!$db->query()) die($db->stderr(true));
							$app->enqueueMessage( JText::_('PLG_VM2BUYPOINTS_AUPRULEADDED_POINTSPURCHASE') );
						}				
						$referencekey = $virtuemart_order_item_id.'|PointspackagePurchase';
						$informationdata = JText::_('PLG_VM2BUYPOINTS_AUPREFUNDRULENAME');
						$api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
						if ( file_exists($api_AUP)){
							require_once ($api_AUP);
							$aupid = AlphaUserPointsHelper::getAnyUserReferreID( $virtuemart_user_id );
							if ( $aupid ) AlphaUserPointsHelper::newpoints( 'plgaup_vm2buypoints_purchase', $aupid, $referencekey , $informationdata , $pts2credit );
						} 
						else{
							JError::raiseWarning( 100, JText::_('PLG_VM2BUYPOINTS_AUPNOTINSTALLED'));
						}
					}
					if($pointsystem ==2){ // Jomsocial
						$q ="UPDATE `#__community_users` SET `points` = `points` + ".$pts2credit ." WHERE userid='".$virtuemart_user_id ;
						$db->setQuery($q);
						if (!$db->query()) die($db->stderr(true));	
						$app->enqueueMessage( $pts2credit.' '.JText::_( 'PLG_VM2BUYPOINTS_CREDITED' ) );	
					}
					elseif($pointsystem ==3){ // EasySocial
						$file 	= JPATH_ROOT . '/administrator/components/com_easysocial/includes/foundry.php';
						if ( file_exists($file)){	
							$db = &JFactory::getDBO();
							$q ="SELECT id FROM #__social_points WHERE extension ='plg_vmpayment_vm2buypoints' AND command='virtuemart.pointspackage' ";
							$db->setQuery($q);
							$points_id = $db->loadResult();
							if(!$points_id){
								$q = "INSERT INTO #__social_points 
								(`command` , `extension` , `title` , `description` , `alias` , `created` , `interval` , `points` , `state`)  VALUES ('virtuemart.pointspackage','plg_vmpayment_vm2buypoints', 'COM_EASYSOCIAL_POINTS_VM2BUYPOINTS_PURCHASEDPOINTS' , 'COM_EASYSOCIAL_POINTS_VM2BUYPOINTS_PURCHASEDPOINTS_DESC' , 'purchase-pointspackage' , '".date('Y-m-d H:i:s')."' , '0' , '0' , '1' )";
								$db->setQuery($q);
								if (!$db->query()) die($db->stderr(true));
								$points_id = $db->insertid();
								$app->enqueueMessage( JText::_('PLG_VM2BUYPOINTS_ESRULEADDED_POINTSPURCHASE') );	
							}
								// we check if has not been paid allready
							$q ="SELECT COUNT(*) FROM `#__social_points_history` WHERE user_id='".$virtuemart_user_id."' AND points='".$pts2credit."' AND message='".$virtuemart_order_item_id. "|PointspackagePurchase' ";
							$db->setQuery($q);
							$yet = $db->loadresult();	
							if($yet<1){
								$q = " INSERT INTO `#__social_points_history` 
								(`points_id` , `user_id` , `points` , `created`, `state` , `message` )
								VALUES 	('".$points_id."' , '".$virtuemart_user_id."' , '".$pts2credit."' , '".date('Y-m-d H:i:s')."' , '1' , '".$virtuemart_order_item_id. "|PointspackagePurchase' )";
								$db->setQuery($q);
								if (!$db->query()) die($db->stderr(true));
								$app->enqueueMessage( JText::_('PLG_VM2BUYPOINTS_THANKYOUFORYOURORDER') );
								$app->enqueueMessage( $pts2credit. ' '.JText::_('PLG_VM2BUYPOINTS_CREDITED') );
							}
						}
						else
							JError::raiseWarning( 100, JText::_('PLG_VM2BUYPOINTS_EASYSOCIANOTINSTALLED'));	
					}	
				} 		
			
		   }				
		}	
		
		
		
		if ( $_formData->order_status == 'X' || $_formData->order_status == 'R' ){  // cancelled or refund
			$q ="SELECT `virtuemart_user_id` FROM `#__virtuemart_orders` WHERE `virtuemart_order_id` ='".$virtuemart_order_id."'  " ;		
			$db->setQuery($q);
			$virtuemart_user_id  = $db->loadResult();

			$q = "SELECT `order_number` FROM `#__virtuemart_orders` WHERE `virtuemart_order_id` ='".$virtuemart_order_id."'";
			$db->setQuery($q);
			
			
			$order_number = $db->loadresult();

			//update contributor details 
			$q = "SELECT projectowner_user_id FROM `#__aup_contributor_details` WHERE `orderid` ='".$order_number."'";
			$db->setQuery($q);

				if($db->loadResult())
				{
					$virtuemart_user_id = $db->loadResult();

					$q ="UPDATE `#__aup_contributor_details` SET `status` = 'cancelled_refunded' WHERE `orderid`='".$order_number."'"  ;
					$db->setQuery($q);
					if (!$db->query()) die($db->stderr(true));
				}
			
			
			
			$q ="SELECT voi.`virtuemart_order_item_id` , voi.`virtuemart_product_id` ,voi.`order_item_sku` , voi.`order_item_name` , voi.`product_quantity` , voi.`product_item_price` ,
					vpc.`virtuemart_category_id` 
					FROM `#__virtuemart_order_items` voi 
					LEFT JOIN `#__virtuemart_product_categories` vpc ON vpc.`virtuemart_product_id` = voi.`virtuemart_product_id` 
					WHERE `virtuemart_order_id` = '".$virtuemart_order_id."' ";
			$db->setQuery($q);
			$items = $db->loadObjectList();
			foreach ($items as $item){				
				$virtuemart_order_item_id	= $item->virtuemart_order_item_id;
				$virtuemart_category_id 	= $item->virtuemart_category_id;
				$product_item_price 		= $item->product_item_price;
				$product_quantity 			= $item->product_quantity;
				//$product_final_price 		= $item->product_final_price;				
				if ($virtuemart_category_id == $vmcatid ){  // product is a point package
					if($pointsystem ==1){  // AUP
						// check if AUP rule is installed 
						$q ="SELECT COUNT(*) FROM #__alpha_userpoints_rules WHERE plugin_function ='plgaup_vm2buypoints_refund' ";
						$db->setQuery($q);
						$is_installed = $db->loadresult();
						if($is_installed<1){
							$q = "INSERT INTO `#__alpha_userpoints_rules` ( `rule_name` , `rule_description` , `rule_plugin` , `plugin_function` ,  `access` , `published` , `system` , `autoapproved` , `fixedpoints` , `category` , `displaymsg` , `method`  )
							VALUES ( '".JText::_( 'PLG_VM2BUYPOINTS_AUPREFUNDRULENAME' )."' , '".JText::_( 'PLG_VM2BUYPOINTS_AUPREFUNDRULENAME_DESC' )."' , 'AUP_VM2BUYPOINTS' , 'plgaup_vm2buypoints_refund' , '1' , '1' , '0' , '1' , '0' , 'pu' , '0' , '4' )";
							$db->setQuery($q);
							if (!$db->query()) die($db->stderr(true));
							$app->enqueueMessage( JText::_('PLG_VM2BUYPOINTS_AUPRULEADDED_POINTSPURCHASE') );
						}
						
						$paid_keyreference = $virtuemart_order_item_id .'|PointspackagePurchase';
						$q = "SELECT referreid , points FROM #__alpha_userpoints_details WHERE keyreference='".$paid_keyreference."' ";
						$db->setQuery($q);
						$aup_data = $db->loadRow();
						$referreid = $aup_data[0];
						$points2revert = $aup_data[1];
						if($points2revert>0){
							$api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
							if ( file_exists($api_AUP)){
								require_once ($api_AUP);
								$keyreference =  $virtuemart_order_item_id. "|PointspackageRefund";
								$informationdata= JText::_('PLG_VM2BUYPOINTS_AUPPURCHASERULENAME');
								AlphaUserPointsHelper::newpoints( 'plgaup_vm2buypoints_refund', $referreid ,$keyreference,  $informationdata , -$points2revert);
								$app->enqueueMessage( $points2revert.' '.JText::_('PLG_VM2BUYPOINTS_HAVEBEENDEDUCTED') );
							}
						}
					}
					elseif($pointsystem ==2){  // Jomsocial
						$pts2refund = $product_item_price * $product_quantity * $ratio;
						$q ="UPDATE `#__community_users` SET `points` = `points` - ".$pts2refund ." WHERE userid='".$virtuemart_user_id;
						$db->setQuery($q);
						if (!$db->query()) die($db->stderr(true));	
						$app->enqueueMessage( $pts2refund.' '.JText::_( 'PLG_VM2BUYPOINTS_HAVEBEENDEDUCTED' ) );
					}
					elseif($pointsystem ==3){  // EasySocial
						$file 	= JPATH_ROOT . DS. 'administrator'.DS.'components'.DS.'com_easysocial'.DS.'includes'.DS.'foundry.php';
						if ( file_exists($file) ){	
							$db = &JFactory::getDBO();
							$q ="SELECT id FROM #__social_points WHERE extension ='plg_vmpayment_vm2buypoints' AND command='virtuemart.pointspackagerefund' ";
							$db->setQuery($q);
							$points_id = $db->loadResult();
							if(!$points_id){
								$q = "INSERT INTO #__social_points 
								(`command` , `extension` , `title` , `description` , `alias` , `created` , `interval` , `points` , `state`)  VALUES ('virtuemart.pointspackagerefund','plg_vmpayment_vm2buypoints', 'COM_EASYSOCIAL_POINTS_VM2BUYPOINTS_POINTSPURCHASEREFUND' , 'COM_EASYSOCIAL_POINTS_VM2BUYPOINTS_POINTSPURCHASEREFUND_DESC' , 'refund-pointspackage' , '".date('Y-m-d H:i:s')."' , '0' , '0' , '1' )";
								$db->setQuery($q);
								if (!$db->query()) die($db->stderr(true));
								$points_id = $db->insertid();
								$app->enqueueMessage( JText::_('PLG_VM2BUYPOINTS_ESRULEADDED_POINTSPURCHASEREFUND') );	
							}
							// get the order item points amount recived
							$q ="SELECT points FROM #__social_points_history WHERE message='".$virtuemart_order_item_id."|PointspackagePurchase' AND state='1' ";
							$db->setQuery($q);
							$pts2refund= $db->loadResult();
							if($pts2refund>0){
							// we check if has not been paid allready
								$q ="SELECT COUNT(*) FROM `#__social_points_history` WHERE user_id='".$virtuemart_user_id."' AND points='-".$pts2refund."' AND message='".$virtuemart_order_item_id."|PointspackageRefund' ";
								$db->setQuery($q);
								$yet = $db->loadresult();
								if($yet<1){
									$q = " INSERT INTO `#__social_points_history` 
									(`points_id` , `user_id` , `points` , `created`, `state` , `message` )
									VALUES 	('".$points_id."' , '".$virtuemart_user_id."' , '-".$pts2refund."' , '".date('Y-m-d H:i:s')."' , '1' , '".$virtuemart_order_item_id."|PointspackageRefund' )";
									$db->setQuery($q);
									if (!$db->query()) die($db->stderr(true));
									$app->enqueueMessage( $pts2refund. ' '.JText::_('PLG_VM2BUYPOINTS_HAVEBEENDEDUCTED') );
								}
							}
							else 
								$app->enqueueMessage( $pts2refund. ' '.JText::_('PLG_VM2BUYPOINTS_NOPOINTSTOREFUND') );
						}
						else
							JError::raiseWarning( 100, JText::_('PLG_VM2BUYPOINTS_EASYSOCIANOTINSTALLED'));	
					}
				}
			}
		} 
	}
}