<?php
if( ! defined( '_VALID_MOS' ) && ! defined( '_JEXEC' ) )
	die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;
/**
 * @version $Id: alphauserpoints.php,v 2.0
 *
 * @author Adrien Roussel
 * @version $Id: standard.php 3681 2011-12-21 12:27:36Z 
 * @package VirtueMart
 * @subpackage payment
 * @copyright Copyright (C) 2007-2011 Nordmograph - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
 *
 * http://virtuemart.org
 */
 
 
 
if (!class_exists('vmPSPlugin'))
    require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

class plgVmPaymentAlphauserpoints extends vmPSPlugin {
	
	// instance of class
    public static $_this = false;

    function __construct(& $subject, $config) {
	parent::__construct($subject, $config);
		
		$this->_loggable = true;
	    $this->tableFields = array_keys($this->getTableSQLFields());
	    $varsToPush = array('aup_ratio' => array('1', 'char'),
		'payment_logos' => array('', 'char'),
		'status_success' => array('C', 'char'),
	    'status_canceled' => array('X', 'char'),
		'countries' => array(0, 'int'),
		'payment_order_total' => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\' ',
		'payment_currency' =>  array(0, 'int'),
		'min_amount' => array(0, 'int'),
		'max_amount' => array(0, 'int'),
		'cost_per_transaction' => array(0, 'int'),
		'cost_percent_total' => array(0, 'int'),
		'tax_id' => array(0, 'int'),
		'partial_payment' => array(0, 'int'),
		'payment_info' => array('', 'string')
	    );

	    $this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
	}
	
	protected function getVmPluginCreateTableSQL() {
		return $this->createTableSQL('Payment Alphauserpoints Table');
    }

	
	
	function getTableSQLFields() {
		$SQLfields = array(
			'id' => 'tinyint(1) unsigned NOT NULL AUTO_INCREMENT',
			'virtuemart_order_id' => 'int(11) UNSIGNED DEFAULT NULL',
			'order_number' => 'char(32) DEFAULT NULL',
			'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED DEFAULT NULL',
			'payment_name' => 'char(255) NOT NULL DEFAULT \'\' ',
			'payment_order_total' => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\' ',
			'payment_currency' => 'char(3) ',
			'cost_per_transaction' => ' decimal(10,2) DEFAULT NULL ',
			'cost_percent_total' => ' decimal(10,2) DEFAULT NULL ',
			'tax_id' => 'smallint(11) DEFAULT NULL'
		);
		return $SQLfields;
    }


	function plgVmOnConfirmedOrderStorePaymentData($virtuemart_order_id, VirtueMartCart $cart, $priceData) {
        return false;
    }

	function plgVmConfirmedOrder($cart, $order) {
		if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
			return null; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($method->payment_element)) {
			return false;
		}
		$lang = JFactory::getLanguage();
		$app = JFactory::getApplication();
		$filename = 'com_virtuemart';
		$lang->load($filename, JPATH_ADMINISTRATOR);
		$vendorId = 0;
	
		$html = "";
	
		if (!class_exists('VirtueMartModelOrders'))
			require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
		$this->getPaymentCurrency($method);
		
		// END printing out HTML Form code (Payment Extra Info)
		$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $method->payment_currency . '" ';
		$db = &JFactory::getDBO();
		$db->setQuery($q);
		$currency_code_3 = $db->loadResult();
		$paymentCurrency = CurrencyDisplay::getInstance($method->payment_currency);
		$totalInPaymentCurrency = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $order['details']['BT']->order_total, false), 2);
		$cd = CurrencyDisplay::getInstance($cart->pricesCurrency);
	
	
		$this->_virtuemart_paymentmethod_id = $order['details']['BT']->virtuemart_paymentmethod_id;
		$dbValues['payment_name'] = $this->renderPluginName($method);
		$dbValues['order_number'] = $order['details']['BT']->order_number;
		$dbValues['virtuemart_paymentmethod_id'] = $this->_virtuemart_paymentmethod_id;
		$dbValues['cost_per_transaction'] = $method->cost_per_transaction;
		$dbValues['cost_percent_total'] = $method->cost_percent_total;
		$dbValues['payment_currency'] = $currency_code_3 ;
		$dbValues['payment_order_total'] = $totalInPaymentCurrency;
		$dbValues['tax_id'] = $method->tax_id;
		
		$this->storePSPluginInternalData($dbValues);

		$aup_ratio = $method->aup_ratio;
	

	
		$new_status = false;
		if (!empty($aup_ratio) ){
			$html = '<table width="100%">' . "\n";
			$html .= $this->getHtmlRow('AUP_PAYMENT_INFO', $dbValues['payment_name']);
			if (!empty($payment_info)) {
				$lang = & JFactory::getLanguage();
		   		if ($lang->hasKey($method->payment_info)) {
					$payment_info = JTExt::_($method->payment_info);
		   		} else {
					$payment_info =  $method->payment_info;
		   		}
				$html .= $this->getHtmlRow('AUP_PAYMENTINFO', $payment_info);
			}
			if (!class_exists('VirtueMartModelCurrency'))
				require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'currency.php');
			$currency = CurrencyDisplay::getInstance('', $order['details']['BT']->virtuemart_vendor_id);
			$html .= $this->getHtmlRow('STANDARD_ORDER_NUMBER', $order['details']['BT']->order_number);
			$html .= $this->getHtmlRow('STANDARD_AMOUNT', $currency->priceDisplay($order['details']['BT']->order_total));
			//$html .= $this->getHtmlRow('STANDARD_AMOUNT', $totalInPaymentCurrency.' '.$currency_code_3);
			$html .= '</table>' ."\n";
	
	
			$virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order['details']['BT']->order_number);
			$order_total =  $totalInPaymentCurrency;
			$order_total = $order_total * $aup_ratio ;
			$_db = JFactory::getDBO();
			$user = JFactory::getUser();
			$q ="SELECT `points` FROM `#__alpha_userpoints` WHERE `userid`='".$user->id."' ";
			$_db->setQuery($q);
			$points = $_db->loadResult();
			if (!class_exists('VirtueMartModelOrders'))
				require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
						
			if ($order_total > $points){
				if($method->partial_payment)
				{
					// negative price product or coupon ?
					
					
					
					
					
					$api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
					if ( file_exists($api_AUP)){	
						require_once ($api_AUP);
						//$keyreference = AlphaUserPointsHelper::buildKeyreference('plgaup_vm_purchase', $virtuemart_order_id. "|AUPay" );
						$keyreference =  $virtuemart_order_id. "|AUPay" ;
						//$app->enqueueMessage('plgaup_vm_purchase,' .$keyreference.', ,'. JText::_('VMPAYMENT_AUP_ORDERNUMBER').': '.$order['details']['BT']->order_number.', -'.$order_total);
						AlphaUserPointsHelper::newpoints(
						 'plgaup_vm_purchase',
						 '', 
						  $keyreference,
						   JText::_('VMPAYMENT_AUP_PARTIALPAYMENT_ORDERNUMBER').': '.$order['details']['BT']->order_number,
							-$points
							);
	
						$modelOrder = new VirtueMartModelOrders();
						$order['order_status'] = $method->status_pending;
						$order['virtuemart_order_id'] = $virtuemart_order_id;
						$order['customer_notified'] = 0;
						$order['comments'] = JTExt::sprintf('VMPAYMENT_AUP_PARTIALPAYMENT_ORDERNUMBER', $order['details']['BT']->order_number);
						
					//	return $this->processConfirmedOrderPaymentResponse(true, $cart, $order, $html, $method->status_success);
						$coupon_price = $points/$aup_ratio;
						$coupon_code = 'AUP-'.$virtuemart_order_id."-".time().'-'.$points.'pts';
						$db = JFactory::getDBO();
						$q ="INSERT INTO #__virtuemart_coupons 
						(coupon_used , coupon_code, percent_or_total , coupon_type, coupon_value, coupon_start_date,  created_on)
						VALUES 
						('0' , '".$coupon_code."' , 'total' , 'gift' , '". $coupon_price ."',  '0000-00-00 00:00:00' , '".date('Y-m-d H:i:s')."')";
						$db->setQuery($q);
						if (!$db->query()) die($db->stderr(true));
						
						$q="UPDATE #__virtuemart_orders SET coupon_discount='".$coupon_price."' , coupon_code='".$coupon_code."' WHERE virtuemart_order_id='".$virtuemart_order_id."'";
						$db->setQuery($q);
						if (!$db->query()) die($db->stderr(true));

						//if(!class_exists('VirtueMartCart')) require JPATH_ROOT.'/components/com_virtuemart/helpers/cart.php';
						//$cart = VirtueMartCart::getCart();
					//	if(empty($cart)) return;
						//if(!empty($cart->couponCode)) return; // commented out so coupon can be validated after and update/delete/add
						
						$cart->setCouponCode($coupon_code);
						//$cart->getCartPrices(); 
						
					$app->enqueueMessage(JText::_('VMPAYMENT_AUP_PARTIALPAYMENT_EXPLAINED'));
						
						return $this->processConfirmedOrderPaymentResponse(false, $cart, $order, $html, $dbValues['payment_name'],$method->status_pending);
					}
					else{
						
						$modelOrder = new VirtueMartModelOrders();
						$order['order_status'] = $method->status_canceled;
						$order['virtuemart_order_id'] = $virtuemart_order_id;
						$order['customer_notified'] = 0;
						$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
						JError::raiseWarning(500, JText::_('VMPAYMENT_AUP_AUPMUSTBE') );
						return $this->processConfirmedOrderPaymentResponse(false, $cart, $order);
					}	
	
				}
				else
				{
					JError::raiseWarning(500, JText::_('VMPAYMENT_AUP_NOTENOUGH').' ('.JText::_('VMPAYMENT_AUP_ONLY').' '.$points.') '.JText::_('VMPAYMENT_AUP_FORYOUR').' ('.$order_total.'). '.JText::_('VMPAYMENT_AUP_CANCELED'));
					
					$modelOrder = new VirtueMartModelOrders();
					$order['order_status'] = $method->status_canceled;
					$order['virtuemart_order_id'] = $virtuemart_order_id;
					$order['customer_notified'] = 0;
					$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
					
					return $this->processConfirmedOrderPaymentResponse(false, $cart, $order, '');
				}
			}
			else{
				$api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
				if ( file_exists($api_AUP)){	
					require_once ($api_AUP);
					//$keyreference = AlphaUserPointsHelper::buildKeyreference('plgaup_vm_purchase', $virtuemart_order_id. "|AUPay" );
					$keyreference =  $virtuemart_order_id. "|AUPay" ;
					//$app->enqueueMessage('plgaup_vm_purchase,' .$keyreference.', ,'. JText::_('VMPAYMENT_AUP_ORDERNUMBER').': '.$order['details']['BT']->order_number.', -'.$order_total);
					AlphaUserPointsHelper::newpoints(
					 'plgaup_vm_purchase',
					 '', 
					  $keyreference,
					   
					   JText::_('VMPAYMENT_AUP_ORDERNUMBER').': '.$order['details']['BT']->order_number,
					    -$order_total
						);

					$modelOrder = new VirtueMartModelOrders();
					$order['order_status'] = $method->status_success;
					$order['virtuemart_order_id'] = $virtuemart_order_id;
					$order['customer_notified'] = 0;
					$order['comments'] = JTExt::sprintf('VMPAYMENT_AUP_ORDERNUMBER', $order['details']['BT']->order_number);
					$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
				//	return $this->processConfirmedOrderPaymentResponse(true, $cart, $order, $html, $method->status_success);
					return $this->processConfirmedOrderPaymentResponse(true, $cart, $order, $html, $dbValues['payment_name'],$method->status_success);
				}
				else{
					
					$modelOrder = new VirtueMartModelOrders();
					$order['order_status'] = $method->status_canceled;
					$order['virtuemart_order_id'] = $virtuemart_order_id;
					$order['customer_notified'] = 0;
					$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
					JError::raiseWarning(500, JText::_('VMPAYMENT_AUP_AUPMUSTBE') );
					return $this->processConfirmedOrderPaymentResponse(false, $cart, $order);
				}
			}
		}
	}
	
        /*
        function plgVmOnPaymentResponseReceived( $pelement)  {
           return null;
       }
*/
	/**
	 * Display stored payment data for an order
	 * @see components/com_virtuemart/helpers/vmPaymentPlugin::plgVmOnShowOrderPaymentBE()
	 */

	
	 
	
	/**
     * Display stored payment data for an order
     *
     */
    function plgVmOnShowOrderBEPayment($virtuemart_order_id, $virtuemart_payment_id) {
		if (!$this->selectedThisByMethodId($virtuemart_payment_id)) {
			return null; // Another method was selected, do nothing
		}
	
		$db = JFactory::getDBO();
		$q = 'SELECT * FROM `' . $this->_tablename . '` '
			. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
		$db->setQuery($q);
		if (!($paymentTable = $db->loadObject())) {
			vmWarn(500, $q . " " . $db->getErrorMsg());
			return '';
		}
		$this->getPaymentCurrency($paymentTable);
	
		$html = '<table class="adminlist">' . "\n";
		$html .=$this->getHtmlHeaderBE();
		$html .= $this->getHtmlRowBE('AUP_PAYMENT_NAME', $paymentTable->payment_name);
		$html .= $this->getHtmlRowBE('AUP_PAYMENT_TOTAL_CURRENCY', $paymentTable->payment_order_total.' '.$paymentTable->payment_currency);
		$html .= '</table>' . "\n";
		return $html;
		}
	
		function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
		if (preg_match('/%$/', $method->cost_percent_total)) {
			$cost_percent_total = substr($method->cost_percent_total, 0, -1);
		} else {
			$cost_percent_total = $method->cost_percent_total;
		}
		return ($method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01));
    }
	
	
	
	 /**
     * Check if the payment conditions are fulfilled for this payment method
     * @author: Valerie Isaksen
     *
     * @param $cart_prices: cart prices
     * @param $payment
     * @return true: if the conditions are fulfilled, false otherwise
     *
     */
     protected function checkConditions($cart, $method, $cart_prices) {

// 		$params = new JParameter($payment->payment_params);
		$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);
	
		$amount = $cart_prices['salesPrice'];
		$amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount
			OR
			($method->min_amount <= $amount AND ($method->max_amount == 0) ));
		if (!$amount_cond) {
			return false;
		}
		$countries = array();
		if (!empty($method->countries)) {
			if (!is_array($method->countries)) {
			$countries[0] = $method->countries;
			} else {
			$countries = $method->countries;
			}
		}
	
		// probably did not gave his BT:ST address
		if (!is_array($address)) {
			$address = array();
			$address['virtuemart_country_id'] = 0;
		}
	
		if (!isset($address['virtuemart_country_id']))
			$address['virtuemart_country_id'] = 0;
		if (count($countries) == 0 || in_array($address['virtuemart_country_id'], $countries) || count($countries) == 0) {
			return true;
		}
	
		return false;
    }
	
	
	/*
     * We must reimplement this triggers for joomla 1.7
     */

    /**
     * Create the table for this plugin if it does not yet exist.
     * This functions checks if the called plugin is active one.
     * When yes it is calling the standard method to create the tables
     * @author Val�rie Isaksen
     *
     */
    function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {
	return $this->onStoreInstallPluginTable($jplugin_id);
    }

    /**
     * This event is fired after the payment method has been selected. It can be used to store
     * additional payment info in the cart.
     *
     * @author Max Milbers
     * @author Val�rie isaksen
     *
     * @param VirtueMartCart $cart: the actual cart
     * @return null if the payment was not selected, true if the data is valid, error message if the data is not vlaid
     *
     */
    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
	return $this->OnSelectCheck($cart);
    }

    /**
     * plgVmDisplayListFEPayment
     * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for exampel
     *
     * @param object $cart Cart object
     * @param integer $selected ID of the method selected
     * @return boolean True on succes, false on failures, null when this plugin was not selected.
     * On errors, JError::raiseWarning (or JError::raiseError) must be used to set a message.
     *
     * @author Valerie Isaksen
     * @author Max Milbers
     */
    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
	return $this->displayListFE($cart, $selected, $htmlIn);
    }

    /*
     * plgVmonSelectedCalculatePricePayment
     * Calculate the price (value, tax_id) of the selected method
     * It is called by the calculator
     * This function does NOT to be reimplemented. If not reimplemented, then the default values from this function are taken.
     * @author Valerie Isaksen
     * @cart: VirtueMartCart the current cart
     * @cart_prices: array the new cart prices
     * @return null if the method was not selected, false if the shiiping rate is not valid any more, true otherwise
     *
     *
     */

    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
	return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }

    function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId) {

	if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
	    return null; // Another method was selected, do nothing
	}
	if (!$this->selectedThisElement($method->payment_element)) {
	    return false;
	}
	 $this->getPaymentCurrency($method);

	$paymentCurrencyId = $method->payment_currency;
    }

    /**
     * plgVmOnCheckAutomaticSelectedPayment
     * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
     * The plugin must check first if it is the correct type
     * @author Valerie Isaksen
     * @param VirtueMartCart cart: the cart object
     * @return null if no plugin was found, 0 if more then one plugin was found,  virtuemart_xxx_id if only one plugin is found
     *
     */
    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
	return $this->onCheckAutomaticSelected($cart, $cart_prices);
    }

    /**
     * This method is fired when showing the order details in the frontend.
     * It displays the method-specific data.
     *
     * @param integer $order_id The order ID
     * @return mixed Null for methods that aren't active, text (HTML) otherwise
     * @author Max Milbers
     * @author Valerie Isaksen
     */
    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
	$this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }

    /**
     * This event is fired during the checkout process. It can be used to validate the
     * method data as entered by the user.
     *
     * @return boolean True when the data was valid, false otherwise. If the plugin is not activated, it should return null.
     * @author Max Milbers

      public function plgVmOnCheckoutCheckDataPayment(  VirtueMartCart $cart) {
      return null;
      }
     */

    /**
     * This method is fired when showing when priting an Order
     * It displays the the payment method-specific data.
     *
     * @param integer $_virtuemart_order_id The order ID
     * @param integer $method_id  method used for this order
     * @return mixed Null when for payment methods that were not selected, text (HTML) otherwise
     * @author Valerie Isaksen
     */
    function plgVmonShowOrderPrintPayment($order_number, $method_id) {
	return $this->onShowOrderPrint($order_number, $method_id);
    }

    function plgVmDeclarePluginParamsPayment($name, $id, &$data) {
	return $this->declarePluginParams('payment', $name, $id, $data);
    }

    function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
	return $this->setOnTablePluginParams($name, $id, $table);
    }
	
	
	
	public function plgVmOnUpdateOrderPayment(  $_formData) {
	 	if ( $_formData->order_status == 'X' || $_formData->order_status == 'R' ){  // cancelled or refund
			$app = JFactory::getApplication();
			$db	   = JFactory::getDBO();
			$paid_keyreference = $_formData->virtuemart_order_id . "|AUPay";
			$q = "SELECT referreid , points FROM #__alpha_userpoints_details WHERE keyreference='".$paid_keyreference."' ";
			$db->setQuery($q);
			$aup_data = $db->loadRow();
			$referreid = $aup_data[0];
			$points2revert = -$aup_data[1];
			if($points2revert>0){
				$api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
				if ( file_exists($api_AUP)){
					require_once ($api_AUP);
					$keyreference = AlphaUserPointsHelper::buildKeyreference('plgaup_vm_auprefund', $virtuemart_order_id. "|AUPrefund" );
					AlphaUserPointsHelper::newpoints( 'plgaup_vm_auprefund', $referreid ,$keyreference,  JText::_('VMPAYMENT_AUP_REFUNDORDERNUMBER').': '.$order['details']['BT']->order_number, $points2revert);
				}
			}
		}
     }
	  
	  
	  

    //Notice: We only need to add the events, which should work for the specific plugin, when an event is doing nothing, it should not be added

    /**
     * Save updated order data to the method specific table
     *
     * @param array $_formData Form data
     * @return mixed, True on success, false on failures (the rest of the save-process will be
     * skipped!), or null when this method is not actived.
     * @author Oscar van Eijk
     *
     

      /**
     * Save updated orderline data to the method specific table
     *
     * @param array $_formData Form data
     * @return mixed, True on success, false on failures (the rest of the save-process will be
     * skipped!), or null when this method is not actived.
     * @author Oscar van Eijk
     *
      public function plgVmOnUpdateOrderLine(  $_formData) {
      return null;
      }

      /**
     * plgVmOnEditOrderLineBE
     * This method is fired when editing the order line details in the backend.
     * It can be used to add line specific package codes
     *
     * @param integer $_orderId The order ID
     * @param integer $_lineId
     * @return mixed Null for method that aren't active, text (HTML) otherwise
     * @author Oscar van Eijk
     *
      public function plgVmOnEditOrderLineBEPayment(  $_orderId, $_lineId) {
      return null;
      }

      /**
     * This method is fired when showing the order details in the frontend, for every orderline.
     * It can be used to display line specific package codes, e.g. with a link to external tracking and
     * tracing systems
     *
     * @param integer $_orderId The order ID
     * @param integer $_lineId
     * @return mixed Null for method that aren't active, text (HTML) otherwise
     * @author Oscar van Eijk
     *
      public function plgVmOnShowOrderLineFE(  $_orderId, $_lineId) {
      return null;
      }

      /**
     * This event is fired when the  method notifies you when an event occurs that affects the order.
     * Typically,  the events  represents for payment authorizations, Fraud Management Filter actions and other actions,
     * such as refunds, disputes, and chargebacks.
     *
     * NOTE for Plugin developers:
     *  If the plugin is NOT actually executed (not the selected payment method), this method must return NULL
     *
     * @param $return_context: it was given and sent in the payment form. The notification should return it back.
     * Used to know which cart should be emptied, in case it is still in the session.
     * @param int $virtuemart_order_id : payment  order id
     * @param char $new_status : new_status for this order id.
     * @return mixed Null when this method was not selected, otherwise the true or false
     *
     * @author Valerie Isaksen
     *
     *
      public function plgVmOnPaymentNotification() {
      return null;
      }

      /**
     * plgVmOnPaymentResponseReceived
     * This event is fired when the  method returns to the shop after the transaction
     *
     *  the method itself should send in the URL the parameters needed
     * NOTE for Plugin developers:
     *  If the plugin is NOT actually executed (not the selected payment method), this method must return NULL
     *
     * @param int $virtuemart_order_id : should return the virtuemart_order_id
     * @param text $html: the html to display
     * @return mixed Null when this method was not selected, otherwise the true or false
     *
     * @author Valerie Isaksen
     *
     *
      function plgVmOnPaymentResponseReceived(, &$virtuemart_order_id, &$html) {
      return null;
      }
     */

 
}

// No closing tag