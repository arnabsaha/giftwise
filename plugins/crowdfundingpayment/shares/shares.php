<?php
/**
 * @package         CrowdFundingShare
 * @subpackage      Plugins
 * @author          Todor Iliev
 * @copyright       Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license         http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * CrowdFunding Shares Plugin
 *
 * @package        CrowdFundingShare
 * @subpackage     Plugins
 */
class plgCrowdFundingPaymentShares extends JPlugin
{
    /**
     * A JRegistry object holding the parameters for the plugin
     *
     * @var    Joomla\Registry\Registry
     * @since  1.5
     */
    public $params = null;

    /**
     * This trigger is executed when payment is completed.
     *
     * @param object                   $context
     * @param object                   $transaction Transaction data
     * @param Joomla\Registry\Registry $params      Component parameters
     * @param object                   $project     Project data
     * @param object                   $reward      Reward data
     */
    public function onAfterPayment($context, &$transaction, &$params, &$project, &$reward)
    {
        // If transaction has been made by anonymous user, stop here.
        if (!$transaction->investor_id) {
            return;
        }

        $app = JFactory::getApplication();
        /** @var $app JApplicationSite */

        if ($app->isAdmin()) {
            return;
        }

        $doc = JFactory::getDocument();
        /**  @var $doc JDocumentRaw */

        // Check document type
        $docType = $doc->getType();
        if (strcmp("raw", $docType) != 0) {
            return;
        }

        if (0 != strpos("com_crowdfunding.notify", $context)) {
            return;
        }

        $db = JFactory::getDbo();

        $typeId = $this->params->get("type_id", 0);

        // Validate type
        jimport("crowdfundingshares.type");
        $type = new CrowdFundingSharesType($db);
        $type->load($typeId);
        if (!$type->getId() or !$type->isPublished()) {
            return;
        }

        // SHARES : Increase the number of shares
        jimport("crowdfundingshares.shares");
        $shares = new CrowdFundingSharesShares($db);
        $keys   = array(
            "user_id"    => (int)$transaction->investor_id,
            "project_id" => (int)$transaction->project_id,
            "type_id"    => (int)$typeId
        );
        $shares->load($keys);

        // If it is a new record, set data about project and user.
        if (!$shares->getId()) {
            $shares->setProjectId($transaction->project_id);
            $shares->setUserId($transaction->investor_id);
            $shares->setTypeId($typeId);
        }

        $numberOfShares = abs($transaction->txn_amount);

        $shares->increase($numberOfShares);
        $shares->store();

        // TRANSACTION : Save an information about transaction for shares
        jimport("crowdfundingshares.transaction");
        $txnShares             = new CrowdFundingSharesTransaction($db);
        $txnShares->setShares($numberOfShares);
        $txnShares->setTypeId($typeId);
        $txnShares->setTransactionId($transaction->id);
        $txnShares->setProjectId($transaction->project_id);
        $txnShares->setBuyerId($transaction->investor_id);
        $txnShares->setSellerId($project->user_id);
        $txnShares->setStatus($transaction->txn_status);

        $txnShares->store();

        // Send mail to the administrator and investor.
        $this->sendMails($project, $transaction->investor_id, $numberOfShares);
    }

    /**
     * This method is invoked when the administrator changes transaction status from the backend.
     *
     * @param string  $context  This string gives information about that where it has been executed the trigger.
     * @param object  $item  A transaction data.
     * @param string  $oldStatus  Old status
     * @param string  $newStatus  New status
     *
     * @return void
     */
    public function onTransactionChangeState($context, &$item, $oldStatus, $newStatus)
    {
        // If transaction has been made by anonymous user, stop here.
        if (!$item->investor_id) {
            return;
        }

        $allowedContexts = array("com_crowdfunding.transaction", "com_crowdfundingfinance.transaction");
        if (!in_array($context, $allowedContexts)) {
            return;
        }

        $app = JFactory::getApplication();
        /** @var $app JApplicationSite */

        if ($app->isSite()) {
            return;
        }

        $doc = JFactory::getDocument();
        /**  @var $doc JDocumentHtml */

        // Check document type
        $docType = $doc->getType();
        if (strcmp("html", $docType) != 0) {
            return;
        }

        $db = JFactory::getDbo();

        $typeId = $this->params->get("type_id", 0);

        // Validate type
        jimport("crowdfundingshares.type");
        $type = new CrowdFundingSharesType($db);
        $type->load($typeId);
        if (!$type->getId() or !$type->isPublished()) {
            return;
        }

        $numberOfShares = abs($item->txn_amount);

        $keys   = array(
            "user_id"    => (int)$item->investor_id,
            "project_id" => (int)$item->project_id,
            "type_id"    => (int)$typeId
        );

        if (strcmp($oldStatus, "completed") == 0) { // Remove shares if someone change the status from completed to other one.

            jimport("crowdfundingshares.shares");
            $shares = new CrowdFundingSharesShares($db);
            $shares->load($keys);

            if ($shares->getId()) {

                $shares->decrease($numberOfShares);
                $shares->updateShares();

                // Update transaction status
                $this->updateTransactionStatus($item, $newStatus);

            }

        } elseif (strcmp($newStatus, "completed") == 0) { // Add funds if someone change the status to completed.

            jimport("crowdfundingshares.shares");
            $shares = new CrowdFundingSharesShares($db);
            $shares->load($keys);

            if ($shares->getId()) {
                $shares->increase($numberOfShares);
                $shares->updateShares();

                // Update transaction status
                $this->updateTransactionStatus($item, $newStatus);
            }
        }

    }

    protected function updateTransactionStatus($item, $status)
    {
        $keys = array(
            "transaction_id" => $item->id,
            "project_id" => $item->project_id,
            "buyer_id" => $item->investor_id
        );

        jimport("crowdfundingshares.transaction");
        $shares = new CrowdFundingSharesTransaction(JFactory::getDbo());
        $shares->load($keys);

        if ($shares->getId()) {
            $shares->setStatus($status);
            $shares->updateStatus();
        }

    }


    /**
     * Send emails to the administrator, project owner and the user who have made a donation.
     *
     * @param object $project
     * @param object $investorId
     * @param int $numberOfShares
     */
    protected function sendMails($project, $investorId, $numberOfShares)
    {
        $app = JFactory::getApplication();
        /** @var $app JApplicationSite */

        // Get website
        $uri     = JUri::getInstance();
        $website = $uri->toString(array("scheme", "host"));

        $emailMode = $this->params->get("email_mode", "plain");

        // Prepare data for parsing
        $data = array(
            "site_name"      => $app->get("sitename"),
            "site_url"       => JUri::root(),
            "item_title"     => $project->title,
            "item_url"       => $website . JRoute::_(CrowdFundingHelperRoute::getDetailsRoute($project->slug, $project->catslug)),
            "amount"         => $numberOfShares
        );

        // Send mail to the administrator
        $emailId = $this->params->get("admin_mail_id", 0);
        if (!empty($emailId)) {

            jimport("crowdfunding.email");
            $email = new CrowdFundingEmail();
            $email->setDb(JFactory::getDbo());
            $email->load($emailId);

            if (!$email->getSenderName()) {
                $email->setSenderName($app->get("fromname"));
            }
            if (!$email->getSenderEmail()) {
                $email->setSenderEmail($app->get("mailfrom"));
            }

            $recipientName = $email->getSenderName();
            $recipientMail = $email->getSenderEmail();

            // Prepare data for parsing
            $data["sender_name"]     = $email->getSenderName();
            $data["sender_email"]    = $email->getSenderEmail();
            $data["recipient_name"]  = $recipientName;
            $data["recipient_email"] = $recipientMail;

            $email->parse($data);
            $subject = $email->getSubject();
            $body    = $email->getBody($emailMode);

            $mailer = JFactory::getMailer();
            if (strcmp("html", $emailMode) == 0) { // Send as HTML message
                $return = $mailer->sendMail($email->getSenderEmail(), $email->getSenderName(), $recipientMail, $subject, $body, CrowdFundingEmail::MAIL_MODE_HTML);
            } else { // Send as plain text.
                $return = $mailer->sendMail($email->getSenderEmail(), $email->getSenderName(), $recipientMail, $subject, $body, CrowdFundingEmail::MAIL_MODE_PLAIN);
            }

            // Check for an error.
            if ($return !== true) {
                JLog::add(JText::_("PLG_CROWDFUNDINGPAYMENT_SHARES_ERROR_MAIL_SENDING_ADMIN"));
            }

        }

        // Send mail to backer
        $emailId    = $this->params->get("user_mail_id", 0);
        if (!empty($emailId) and !empty($investorId)) {

            $email = new CrowdFundingEmail();
            $email->setDb(JFactory::getDbo());
            $email->load($emailId);

            if (!$email->getSenderName()) {
                $email->setSenderName($app->get("fromname"));
            }
            if (!$email->getSenderEmail()) {
                $email->setSenderEmail($app->get("mailfrom"));
            }

            $user          = JFactory::getUser($investorId);
            $recipientName = $user->get("name");
            $recipientMail = $user->get("email");

            // Prepare data for parsing
            $data["sender_name"]     = $email->getSenderName();
            $data["sender_email"]    = $email->getSenderEmail();
            $data["recipient_name"]  = $recipientName;
            $data["recipient_email"] = $recipientMail;

            $email->parse($data);
            $subject = $email->getSubject();
            $body    = $email->getBody($emailMode);

            $mailer = JFactory::getMailer();
            if (strcmp("html", $emailMode) == 0) { // Send as HTML message
                $return = $mailer->sendMail($email->getSenderEmail(), $email->getSenderName(), $recipientMail, $subject, $body, CrowdFundingEmail::MAIL_MODE_HTML);
            } else { // Send as plain text.
                $return = $mailer->sendMail($email->getSenderEmail(), $email->getSenderName(), $recipientMail, $subject, $body, CrowdFundingEmail::MAIL_MODE_PLAIN);
            }

            // Check for an error.
            if ($return !== true) {
                JLog::add(JText::_("PLG_CROWDFUNDINGPAYMENT_SHARES_ERROR_MAIL_SENDING_USER"));
            }

        }

    }
}
