<?php
/**
 * @package      CrowdFundingData
 * @subpackage   Plugins
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('_JEXEC') or die;

// Load the script that initializes the select element with banks.
$doc->addScript("plugins/crowdfundingpayment/data/js/script.js?v=" . rawurlencode($this->version));

?>
<div class="row-fluid">
    <div class="span12">
        <h2><?php echo JText::_("PLG_CROWDFUNDINGPAYMENT_DATA_INFORMATION_ABOUT_YOU");?></h2>

        <form action="index.php?option=com_crowdfundingdata" method="post" id="js-cfdata-form">

            <fieldset class="well">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel("name"); ?></div>
                    <div class="controls"><?php echo $this->form->getInput("name"); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel("email"); ?></div>
                    <div class="controls"><?php echo $this->form->getInput("email"); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel("address"); ?></div>
                    <div class="controls"><?php echo $this->form->getInput("address"); ?></div>
                </div>

                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel("country_id"); ?></div>
                    <div class="controls"><?php echo $this->form->getInput("country_id"); ?></div>
                </div>


                <div class="control-group">
                    <div class="controls">

                        <div class="alert alert-info hide" id="js-cfdata-btn-alert">
                            <i class="icon icon-info-sign"></i>
                        </div>

                        <button type="submit" class="btn btn-primary" id="js-cfdata-btn-submit">
                            <?php echo JText::_('PLG_CROWDFUNDINGPAYMENT_DATA_SUBMIT'); ?>
                        </button>

                        <a href="#" class="btn btn-success hide" id="js-continue-btn">
                            <?php echo JText::_("PLG_CROWDFUNDINGPAYMENT_DATA_CONTINUE_NEXT_STEP"); ?>
                        </a>

                    </div>
                </div>

                <?php echo $this->form->getInput("project_id"); ?>
                <input type="hidden" name="task" value="record.save" />
                <input type="hidden" name="format" value="raw" />

                <?php if ($componentParams->get("backing_terms", 0) and !empty($this->terms)) { ?>
                    <input type="hidden" name="terms" value="1" />
                <?php } ?>

                <?php echo JHtml::_('form.token'); ?>
            </fieldset>

        </form>
    </div>
</div>


<div class="modal hide fade" id="js-cfdata-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo JText::_('PLG_CROWDFUNDINGPAYMENT_DATA_CONFIRMATION_TITLE'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo JText::_('PLG_CROWDFUNDINGPAYMENT_DATA_CONFIRMATION_QUESTION'); ?></p>
    </div>
    <div class="modal-footer">
        <img src="media/com_crowdfunding/images/ajax-loader.gif" width="16" height="16" id="js-cfdata-ajax-loading" style="display: none;" />
        <button class="btn btn-primary" id="js-cfdata-btn-yes"><?php echo JText::_('JYES'); ?></button>
        <button class="btn" id="js-cfdata-btn-no"><?php echo JText::_('JNO'); ?></button>
    </div>
</div>