jQuery(document).ready(function() {
	 
	jQuery("#js-cfdata-btn-submit").on("click", function(event){
		event.preventDefault();
		jQuery('#js-cfdata-modal').modal("show");
	});

	jQuery("#js-cfdata-btn-yes").on("click", function(event){
		event.preventDefault();

        var form = jQuery("#js-cfdata-form");

        var url = form.attr("action");

		jQuery.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			dataType: "text json",
			cache: false,
			beforeSend: function() {

				// Display ajax loading image
				jQuery("#js-cfdata-ajax-loading").show();
				jQuery("#js-cfdata-btn-yes").prop("disabled", true);
				jQuery("#js-cfdata-btn-no").prop("disabled", true);

			},
			success: function(response) {

				// Hide ajax loading image
				jQuery("#js-cfdata-ajax-loading").hide();

				// Hide the button
				jQuery("#js-cfdata-btn-submit").hide();

                // Display information about process of submition.
                jQuery("#js-cfdata-btn-alert").append(response.text).show();

				// Display the button that points to next step
				if(response.success) {
					jQuery("#js-continue-btn").attr("href", response.redirect_url).show();

				} else {
					if(response.redirect_url) {
						setTimeout("location.href = '"+ response.redirect_url +"'", 1500);
					}
				}

				// Hide modal window
                jQuery('#js-cfdata-modal').modal('hide');

			}

		});

	});

	jQuery("#js-cfdata-btn-no").on("click", function(event){
		event.preventDefault();
		jQuery('#js-cfdata-modal').modal('hide')
	});
});