<?php
/**
 * @package      CrowdFundingPartners
 * @subpackage   Plugins
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2015 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

defined('_JEXEC') or die;

?>
<div class="row-fluid">
    <div class="span12">
        <h4><?php echo JText::_("PLG_CONTENT_CROWDFUNDINGPARTNERS_TEAM");?></h4>

        <table class="table table-bordered mtb_25_0">
            <tbody>
            <?php
            foreach ($partners as $partner) { ?>
                <tr>
                    <td>
                        <?php echo JHtml::_("crowdfundingpartners.partner", $partner); ?>
                    </td>
                </tr>
            <?php
            } ?>
            </tbody>
        </table>

    </div>
</div>