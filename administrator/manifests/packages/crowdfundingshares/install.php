<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Script file of the component
 */
class pkg_crowdfundingsharesInstallerScript
{
    /**
     * Method to install the component.
     *
     * @param $parent
     *
     * @return void
     */
    public function install($parent)
    {
    }

    /**
     * Method to uninstall the component.
     *
     * @param $parent
     *
     * @return void
     */
    public function uninstall($parent)
    {
    }

    /**
     * Method to update the component.
     *
     * @param $parent
     *
     * @return void
     */
    public function update($parent)
    {
    }

    /**
     * Method to run before an install/update/uninstall method
     *
     * @param $type
     * @param $parent
     *
     * @return void
     */
    public function preflight($type, $parent)
    {
    }

    /**
     * Method to run after an install/update/uninstall method
     *
     * @param $type
     * @param $parent
     *
     * @return void
     */
    public function postflight($type, $parent)
    {
        if (!defined("CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR")) {
            define("CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR", JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "com_crowdfundingshares");
        }

        // Register Component helpers
        JLoader::register("CrowdFundingSharesInstallHelper", CROWDFUNDINGSHARES_PATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . "helpers" . DIRECTORY_SEPARATOR . "install.php");

        // Start table with the information
        CrowdFundingSharesInstallHelper::startTable();

        // Requirements
        CrowdFundingSharesInstallHelper::addRowHeading(JText::_("COM_CROWDFUNDINGSHARES_MINIMUM_REQUIREMENTS"));

        // Display result about verification for GD library
        $title = JText::_("COM_CROWDFUNDINGSHARES_GD_LIBRARY");
        $info  = "";
        if (!extension_loaded('gd') and function_exists('gd_info')) {
            $result = array("type" => "important", "text" => JText::_("COM_CROWDFUNDINGSHARES_WARNING"));
        } else {
            $result = array("type" => "success", "text" => JText::_("JON"));
        }
        CrowdFundingSharesInstallHelper::addRow($title, $result, $info);

        // Display result about verification for cURL library
        $title = JText::_("COM_CROWDFUNDINGSHARES_CURL_LIBRARY");
        $info  = "";
        if (!extension_loaded('curl')) {
            $info   = JText::_("COM_CROWDFUNDINGSHARES_CURL_INFO");
            $result = array("type" => "important", "text" => JText::_("JOFF"));
        } else {
            $result = array("type" => "success", "text" => JText::_("JON"));
        }
        CrowdFundingSharesInstallHelper::addRow($title, $result, $info);

        // Display result about verification Magic Quotes
        $title = JText::_("COM_CROWDFUNDINGSHARES_MAGIC_QUOTES");
        $info  = "";
        if (get_magic_quotes_gpc()) {
            $info   = JText::_("COM_CROWDFUNDINGSHARES_MAGIC_QUOTES_INFO");
            $result = array("type" => "important", "text" => JText::_("JON"));
        } else {
            $result = array("type" => "success", "text" => JText::_("JOFF"));
        }
        CrowdFundingSharesInstallHelper::addRow($title, $result, $info);

        // Display result about verification FileInfo
        $title = JText::_("COM_CROWDFUNDINGSHARES_FILEINFO");
        $info  = "";
        if (!function_exists('finfo_open')) {
            $info   = JText::_("COM_CROWDFUNDINGSHARES_FILEINFO_INFO");
            $result = array("type" => "important", "text" => JText::_("JOFF"));
        } else {
            $result = array("type" => "success", "text" => JText::_("JON"));
        }
        CrowdFundingSharesInstallHelper::addRow($title, $result, $info);

        // Display result about verification FileInfo
        $title = JText::_("COM_CROWDFUNDINGSHARES_PHP_VERSION");
        $info  = "";
        if (version_compare(PHP_VERSION, '5.3.0') < 0) {
            $result = array("type" => "important", "text" => JText::_("COM_CROWDFUNDINGSHARES_WARNING"));
        } else {
            $result = array("type" => "success", "text" => JText::_("JYES"));
        }
        CrowdFundingSharesInstallHelper::addRow($title, $result, $info);

        // Display result about verification of installed ITPrism Library
        jimport("itprism.version");
        $title = JText::_("COM_CROWDFUNDINGSHARES_ITPRISM_LIBRARY");
        $info  = "";
        if (!class_exists("ITPrismVersion")) {
            $info   = JText::_("COM_CROWDFUNDINGSHARES_ITPRISM_LIBRARY_DOWNLOAD");
            $result = array("type" => "important", "text" => JText::_("JNO"));
        } else {
            $result = array("type" => "success", "text" => JText::_("JYES"));
        }
        CrowdFundingSharesInstallHelper::addRow($title, $result, $info);

        // Installed extensions

        CrowdFundingSharesInstallHelper::addRowHeading(JText::_("COM_CROWDFUNDINGSHARES_INSTALLED_EXTENSIONS"));

        // CrowdFunding Library
        $result = array("type" => "success", "text" => JText::_("COM_CROWDFUNDINGSHARES_INSTALLED"));
        CrowdFundingSharesInstallHelper::addRow(JText::_("COM_CROWDFUNDINGSHARES_CROWDFUNDINGSHARES_LIBRARY"), $result, JText::_("COM_CROWDFUNDINGSHARES_LIBRARY"));

        // Plugins

        // Content - CrowdFunding - Navigation

        // CrowdFunding Payment - Shares
        $result = array("type" => "success", "text" => JText::_("COM_CROWDFUNDINGSHARES_INSTALLED"));
        CrowdFundingSharesInstallHelper::addRow(JText::_("COM_CROWDFUNDINGSHARES_CROWDFUNDINGPAYMENT_SHARES"), $result, JText::_("COM_CROWDFUNDINGSHARES_PLUGIN"));

        // End table
        CrowdFundingSharesInstallHelper::endTable();

        echo JText::sprintf("COM_CROWDFUNDINGSHARES_MESSAGE_REVIEW_SAVE_SETTINGS", JRoute::_("index.php?option=com_crowdfundingshares"));

        jimport("itprism.version");
        if (!class_exists("ITPrismVersion")) {
            echo JText::_("COM_CROWDFUNDINGSHARES_MESSAGE_INSTALL_ITPRISM_LIBRARY");
        }
    }
}
