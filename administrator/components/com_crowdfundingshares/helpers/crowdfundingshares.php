<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * It is CrowdFunding Shares helper class
 */
class CrowdfundingSharesHelper
{
    protected static $extension = "com_crowdfundingshares";

    /**
     * Configure the Linkbar.
     *
     * @param    string  $vName  The name of the active view.
     *
     * @since    1.6
     */
    public static function addSubmenu($vName = 'dashboard')
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_CROWDFUNDINGSHARES_DASHBOARD'),
            'index.php?option=' . self::$extension . '&view=dashboard',
            $vName == 'dashboard'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_CROWDFUNDINGSHARES_SHARES'),
            'index.php?option=' . self::$extension . '&view=shares',
            $vName == 'shares'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_CROWDFUNDINGSHARES_TRANSACTIONS'),
            'index.php?option=' . self::$extension . '&view=transactions',
            $vName == 'transactions'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_CROWDFUNDINGSHARES_TYPES'),
            'index.php?option=' . self::$extension . '&view=types',
            $vName == 'types'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_CROWDFUNDINGSHARES_PLUGINS'),
            'index.php?option=com_plugins&view=plugins&filter_search=' . rawurlencode("shares"),
            $vName == 'plugins'
        );
    }
}
