<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
defined('JPATH_PLATFORM') or die;

/**
 * Abstract class defining methods that can be
 * implemented by an Observer class of a JTable class (which is an Observable).
 * Attaches $this Observer to the $table in the constructor.
 * The classes extending this class should not be instanciated directly, as they
 * are automatically instanciated by the JObserverMapper
 *
 * @package      UserIdeas
 * @subpackage   Component
 * @link         http://docs.joomla.org/JTableObserver
 * @since        3.1.2
 */
class CrowdFundingSharesObserverType extends JTableObserver
{

    /**
     * The pattern for this table's TypeAlias
     *
     * @var    string
     * @since  3.1.2
     */
    protected $typeAliasPattern = null;

    /**
     * Creates the associated observer instance and attaches it to the $observableObject
     * $typeAlias can be of the form "{variableName}.type", automatically replacing {variableName} with table-instance variables variableName
     *
     * @param   JObservableInterface $observableObject The subject object to be observed
     * @param   array                $params           ( 'typeAlias' => $typeAlias )
     *
     * @return  CrowdFundingSharesObserverType
     *
     * @since   3.1.2
     */
    public static function createObserver(JObservableInterface $observableObject, $params = array())
    {
        $observer = new self($observableObject);

        $observer->typeAliasPattern = JArrayHelper::getValue($params, 'typeAlias');

        return $observer;
    }

    /**
     * Pre-processor for $table->delete($pk)
     *
     * @param   mixed $pk An optional primary key value to delete.  If not set the instance property value is used.
     *
     * @return  void
     *
     * @since   3.1.2
     * @throws  UnexpectedValueException
     */
    public function onAfterDelete($pk)
    {
        if (!empty($pk)) {

            $db = $this->table->getDbo();

            // TRANSACTIONS: Remove transactions
            $query = $db->getQuery(true);
            $query->delete($db->quoteName("#__cfs_transactions"));

            if (is_array($pk)) {
                $query->where($db->quoteName("type_id") . " IN (" . implode(",", $pk) . " )");
            } else {
                $query->where($db->quoteName("type_id") . " = " . (int)$pk);
            }

            $db->setQuery($query);
            $db->execute();

            // SHARES: Remove shares
            $query = $db->getQuery(true);
            $query->delete($db->quoteName("#__cfs_shares"));

            if (is_array($pk)) {
                $query->where($db->quoteName("type_id") . " IN (" . implode(",", $pk) . " )");
            } else {
                $query->where($db->quoteName("type_id") . "=" . (int)$pk);
            }

            $db->setQuery($query);
            $db->execute();

        }
    }
}
