<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;
?>
<tr>
    <th width="1%" class="hidden-phone">
        <?php echo JHtml::_('grid.checkall'); ?>
    </th>
	<th class="title">
	     <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_PROJECT', 'd.title', $this->listDirn, $this->listOrder); ?>
	</th>
	<th width="10%" class="center nowrap">
	     <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_SHARES', 'a.shares', $this->listDirn, $this->listOrder); ?>
	</th>
	<th width="10%" class="center nowrap hidden-phone">
	     <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_SELLER', 'b.name', $this->listDirn, $this->listOrder); ?>
	</th>
	<th width="10%" class="center nowrap hidden-phone">
	     <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_BUYER', 'c.name', $this->listDirn, $this->listOrder); ?>
	</th>
    <th width="10%" class="center nowrap hidden-phone">
        <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_STATUS', 'a.status', $this->listDirn, $this->listOrder); ?>
    </th>
	<th width="10%" class="center nowrap hidden-phone">
	     <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_DATE', 'a.record_date', $this->listDirn, $this->listOrder); ?>
	</th>
    <th width="3%"  class="center nowrap hidden-phone">
        <?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'a.id', $this->listDirn, $this->listOrder); ?>
    </th>
</tr>
	  