<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;
?>
<?php foreach ($this->items as $i => $item) {?>
	<tr class="row<?php echo $i % 2; ?>">
        <td>
            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
        </td>
		<td class="has-context">
            <?php echo $this->escape($item->project); ?>

            <?php if (!empty($item->txn_id)) { ?>
            <div class="small">
                <?php
                $link = '<a href="'.JRoute::_("index.php?option=com_crowdfunding&view=transactions&filter_search=id:". $item->transaction_id).'">'.$this->escape($item->txn_id)."</a>";
                echo JText::sprintf("COM_CROWDFUNDINGSHARES_TRANSACTION_S", $link); ?>
            </div>
            <?php } ?>
        </td>
		<td class="center has-context">
            <?php echo $item->shares; ?>
            <div class="small">( <?php echo $item->shares_type; ?> )</div>
        </td>
		<td class="center hidden-phone">
            <a href="<?php echo JRoute::_("index.php?option=com_crowdfunding&view=users&filter_search=id:" . $item->seller_id); ?>">
            <?php echo $this->escape($item->seller); ?>
            </a>
        </td>
		<td class="center hidden-phone">
            <a href="<?php echo JRoute::_("index.php?option=com_crowdfunding&view=users&filter_search=id:" . $item->buyer_id); ?>">
            <?php echo $this->escape($item->buyer); ?>
            </a>
        </td>
        <td class="center hidden-phone">
            <?php echo $this->escape($item->status); ?>
        </td>
		<td class="center hidden-phone">
            <?php echo JHtml::_('date', $item->record_date, JText::_('DATE_FORMAT_LC3')); ?>
        </td>
        <td class="center hidden-phone">
            <?php echo $item->id;?>
        </td>
	</tr>
<?php }?>
	  