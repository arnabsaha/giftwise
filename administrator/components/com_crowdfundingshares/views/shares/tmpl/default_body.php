<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;
?>
<?php foreach ($this->items as $i => $item) {?>
    <tr class="row<?php echo $i % 2; ?>">
        <td class="center hidden-phone">
            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
        </td>
        <td>
            <a href="<?php echo JRoute::_("index.php?option=com_crowdfundingshares&view=shares&layout=edit&id=" . $item->id); ?>">
            <?php echo $this->escape($item->user); ?>
            </a>
        </td>
        <td class="center has-context">
            <?php echo $item->shares; ?>
            <div class="small">( <?php echo $item->shares_type; ?> )</div>
        </td>
        <td class="hidden-phone">
            <?php echo $this->escape($item->project); ?>
        </td>
        <td class="center hidden-phone">
            <?php echo $item->id;?>
        </td>
    </tr>
<?php }?>
