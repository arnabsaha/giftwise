<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;
?>
<tr>
    <th width="1%" class="hidden-phone">
        <?php echo JHtml::_('grid.checkall'); ?>
    </th>
    <th class="title">
        <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_USER', 'b.name', $this->listDirn, $this->listOrder); ?>
    </th>
    <th width="10%" class="center">
        <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_SHARES', 'a.shares', $this->listDirn, $this->listOrder); ?>
    </th>
    <th width="70%" class="nowrap hidden-phone">
        <?php echo JHtml::_('grid.sort',  'COM_CROWDFUNDINGSHARES_PROJECT', 'c.title', $this->listDirn, $this->listOrder); ?>
    </th>
    <th width="3%" class="center hidden-phone">
        <?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'a.id', $this->listDirn, $this->listOrder); ?>
    </th>
</tr>

	  