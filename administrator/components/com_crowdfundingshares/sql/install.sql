CREATE TABLE IF NOT EXISTS `#__cfs_shares` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `shares` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `type_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_cfs_shares` (`user_id`,`project_id`,`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cfs_transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shares` int(10) unsigned NOT NULL,
  `status` enum('pending','completed','canceled','refunded','failed') NOT NULL DEFAULT 'pending',
  `record_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_id` tinyint(5) unsigned NOT NULL,
  `transaction_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'This is a transaction ID from CrowdFunding platform.',
  `project_id` int(10) unsigned NOT NULL,
  `buyer_id` int(10) unsigned NOT NULL,
  `seller_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cfs_types` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;