<?php
/**
 * @package      CrowdFundingShares
 * @subpackage   Component
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class CrowdfundingSharesModelTransactions extends JModelList
{
    /**
     * Constructor.
     *
     * @param   array  $config An optional associative array of configuration settings.
     *
     * @see     JController
     * @since   1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'a.id',
                'shares', 'a.shares',
                'record_date', 'a.record_date',
                'seller', 'b.name',
                'buyer', 'c.name',
                'project', 'd.title',
                'status', 'a.status',
            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since   1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {
        // Load the component parameters.
        $params = JComponentHelper::getParams($this->option);
        $this->setState('params', $params);

        // Load the filter state.
        $value = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $value);

        // Prepare filter by payment status.
        $value = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status');
        $this->setState('filter.status', $value);

        // Prepare filter by shares type.
        $value = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type');
        $this->setState('filter.type', $value);

        // List state information.
        parent::populateState('a.record_date', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param   string $id A prefix for the store id.
     *
     * @return  string      A store id.
     * @since   1.6
     */
    protected function getStoreId($id = '')
    {
        // Compile the store id.
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.type');
        $id .= ':' . $this->getState('filter.status');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return  JDatabaseQuery
     * @since   1.6
     */
    protected function getListQuery()
    {
        $db = $this->getDbo();
        /** @var $db JDatabaseDriver */

        // Create a new query object.
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
            $this->getState(
                'list.select',
                'a.id, a.shares, a.status, a.record_date, a.type_id, a.transaction_id, a.project_id, a.buyer_id, a.seller_id, ' .
                'b.name AS seller, ' .
                'c.name AS buyer, ' .
                'd.title AS project, ' .
                'e.txn_id, ' .
                'f.title AS shares_type'
            )
        );
        $query->from($db->quoteName('#__cfs_transactions', 'a'));
        $query->innerJoin($db->quoteName('#__users', 'b') . ' ON a.seller_id = b.id');
        $query->innerJoin($db->quoteName('#__users', 'c') . ' ON a.buyer_id = c.id');
        $query->innerJoin($db->quoteName('#__crowdf_projects', 'd') . ' ON a.project_id = d.id');
        $query->leftJoin($db->quoteName('#__crowdf_transactions', 'e') . ' ON a.transaction_id = e.id');
        $query->leftJoin($db->quoteName('#__cfs_types', 'f') . ' ON a.type_id = f.id');

        // Filter by payment status.
        $status = $this->getState('filter.status');
        if (!empty($status)) {
            $query->where('a.status = ' . $db->quote($status));
        }

        // Filter by type.
        $type = $this->getState('filter.type');
        if (!empty($type)) {
            $query->where('a.type_id = ' . $db->quote($type));
        }

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {

            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int)substr($search, 3));
            } elseif (stripos($search, 'sid:') === 0) {
                $query->where('a.seller_id = ' . (int)substr($search, 4));
            } elseif (stripos($search, 'bid:') === 0) {
                $query->where('a.buyer_id = ' . (int)substr($search, 4));
            } elseif (stripos($search, 'pid:') === 0) {
                $query->where('a.project_id = ' . (int)substr($search, 4));
            } elseif (stripos($search, 'tid:') === 0) {
                $query->where('a.transaction_id = ' . (int)substr($search, 4));
            } else {
                $escaped = $db->escape($search, true);
                $quoted  = $db->quote("%" . $escaped . "%", false);
                $query->where('d.title LIKE ' . $quoted);
            }
        }

        // Add the list ordering clause.
        $orderString = $this->getOrderString();
        $query->order($db->escape($orderString));

        return $query;
    }

    protected function getOrderString()
    {
        $orderCol  = $this->getState('list.ordering');
        $orderDirn = $this->getState('list.direction');

        return $orderCol . ' ' . $orderDirn;
    }
}
