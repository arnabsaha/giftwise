<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

JLoader::register("CrowdFundingModelTransaction", CROWDFUNDING_PATH_COMPONENT_ADMINISTRATOR . "/models/transaction.php");

class CrowdFundingFinanceModelTransaction extends CrowdFundingModelTransaction
{
    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param   string $type   The table type to instantiate
     * @param   string $prefix A prefix for the table class name. Optional.
     * @param   array  $config Configuration array for model. Optional.
     *
     * @return  JTable  A database object
     * @since   1.6
     */
    public function getTable($type = 'Transaction', $prefix = 'CrowdFundingFinanceTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }
}
