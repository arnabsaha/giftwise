<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

JLoader::register("CrowdFundingModelProjects", CROWDFUNDING_PATH_COMPONENT_ADMINISTRATOR . "/models/projects.php");

class CrowdFundingFinanceModelProjects extends CrowdFundingModelProjects
{

}
