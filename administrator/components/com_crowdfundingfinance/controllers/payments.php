<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

// jimport('joomla.application.component.controller');
JLoader::register("CrowdFundingControllerPayments", CROWDFUNDING_PATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . "controllers" . DIRECTORY_SEPARATOR . "payments.php");

/**
 * This controller provides functionality
 * that helps to payment plugins to prepare their payment data.
 *
 * @package        CrowdFundingFinance
 * @subpackage     Payments
 *
 */
class CrowdFundingFinanceControllerPayments extends CrowdFundingControllerPayments
{
    protected $text_prefix = "COM_CROWDFUNDINGFINANCE";

    /**
     * Method to get a model object, loading it if required.
     *
     * @param    string $name   The model name. Optional.
     * @param    string $prefix The class prefix. Optional.
     * @param    array  $config Configuration array for model. Optional.
     *
     * @return    object    The model.
     * @since    1.5
     */
    public function getModel($name = 'Payments', $prefix = '', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }
}
