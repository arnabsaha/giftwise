<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla');

/**
 * CrowdFunding Finance project controller class.
 *
 * @package        ITPrism Components
 * @subpackage     CrowdFunding
 * @since          1.6
 */
class CrowdFundingFinanceControllerStatistics extends JControllerLegacy
{

    public function getProjectTransactions()
    {
        // Create response object
        jimport("itprism.response.json");
        $response = new ITPrismResponseJson();

        $app = JFactory::getApplication();
        /** @var $app JApplicationAdministrator */

        $itemId = $app->input->getInt('id');

        // Check for errors.
        if (!$itemId) {
            $response
                ->setTitle(JText::_('COM_CROWDFUNDINGFINANCE_FAIL'))
                ->setText(JText::_('COM_CROWDFUNDINGFINANCE_ERROR_INVALID_PROJECT'))
                ->failure();

            echo $response;
            JFactory::getApplication()->close();
        }

        $data = array();

        try {

            // Get statistics
            jimport("crowdfunding.statistics.project");
            $project = new CrowdFundingStatisticsProject(JFactory::getDbo(), $itemId);
            $data    = $project->getFullPeriodAmounts();

        } catch (Exception $e) {

            JLog::add($e->getMessage());

            $response
                ->setTitle(JText::_('COM_CROWDFUNDINGFINANCE_FAIL'))
                ->setText(JText::_('COM_CROWDFUNDINGFINANCE_ERROR_SYSTEM'))
                ->failure();

            echo $response;
            JFactory::getApplication()->close();

        }

        $response
            ->setData($data)
            ->success();

        echo $response;
        JFactory::getApplication()->close();
    }


    public function getProjectFunds()
    {
        // Create response object
        jimport("itprism.response.json");
        $response = new ITPrismResponseJson();

        $app = JFactory::getApplication();
        /** @var $app JApplicationAdministrator */

        $itemId = $app->input->getInt('id');

        // Check for errors.
        if (!$itemId) {
            $response
                ->setTitle(JText::_('COM_CROWDFUNDINGFINANCE_FAIL'))
                ->setText(JText::_('COM_CROWDFUNDINGFINANCE_ERROR_INVALID_PROJECT'))
                ->failure();

            echo $response;
            JFactory::getApplication()->close();
        }

        try {

            // Get statistics
            jimport("crowdfunding.statistics.project");
            $project = new CrowdFundingStatisticsProject(JFactory::getDbo(), $itemId);
            $data    = $project->getFundedAmount();

        } catch (Exception $e) {

            JLog::add($e->getMessage());

            $response
                ->setTitle(JText::_('COM_CROWDFUNDINGFINANCE_FAIL'))
                ->setText(JText::_('COM_CROWDFUNDINGFINANCE_ERROR_SYSTEM'))
                ->failure();

            echo $response;
            JFactory::getApplication()->close();

            throw new Exception(JText::_('COM_CROWDFUNDINGFINANCE_ERROR_SYSTEM'));

        }

        $response
            ->setData($data)
            ->success();

        echo $response;
        JFactory::getApplication()->close();
    }
}
