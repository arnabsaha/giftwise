<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// No direct access
defined('_JEXEC') or die;

// jimport('itprism.controller.admin');
JLoader::register("CrowdFundingControllerTransactions", CROWDFUNDING_PATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . "controllers" . DIRECTORY_SEPARATOR . "transactions.php");

/**
 * CrowdFunding Finance transactions controller class
 *
 * @package      CrowdFundingFinance
 * @subpackage   Components
 */
class CrowdFundingFinanceControllerTransactions extends CrowdFundingControllerTransactions
{
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    public function getModel($name = 'Transaction', $prefix = 'CrowdFundingFinanceModel', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }
}
