<?php
/**
 * @package      CrowdFundingFinance
 * @subpackage   Components
 * @author       Todor Iliev
 * @copyright    Copyright (C) 2014 Todor Iliev <todor@itprism.com>. All rights reserved.
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

class CrowdFundingFinanceViewDashboard extends JViewLegacy
{
    /**
     * @var JDocumentHtml
     */
    public $document;

    /**
     * @var Joomla\Registry\Registry
     */
    protected $cfParams;

    protected $option;

    protected $latest;
    protected $totalProjects;
    protected $totalTransactions;
    protected $totalAmount;
    protected $currency;
    protected $version;
    protected $itprismVersion;

    protected $sidebar;

    public function display($tpl = null)
    {
        $this->version = new CrowdFundingFinanceVersion();

        // Load ITPrism library version
        jimport("itprism.version");
        if (!class_exists("ITPrismVersion")) {
            $this->itprismVersion = JText::_("COM_CROWDFUNDINGFINANCE_ITPRISM_LIBRARY_DOWNLOAD");
        } else {
            $itprismVersion       = new ITPrismVersion();
            $this->itprismVersion = $itprismVersion->getShortVersion();
        }

        /** @var  $cfParams Joomla\Registry\Registry */
        $cfParams       = JComponentHelper::getParams("com_crowdfunding");
        $this->cfParams = $cfParams;

        // Get latest transactions.
        jimport("crowdfunding.statistics.transactions.latest");
        $this->latest = new CrowdFundingStatisticsTransactionsLatest(JFactory::getDbo());
        $this->latest->load(5);

        jimport("crowdfunding.statistics.basic");
        $basic                   = new CrowdFundingStatisticsBasic(JFactory::getDbo());
        $this->totalProjects     = $basic->getTotalProjects();
        $this->totalTransactions = $basic->getTotalTransactions();
        $this->totalAmount       = $basic->getTotalAmount();

        // Get currency.
        jimport("crowdfunding.currency");
        $currencyId     = $this->cfParams->get("project_currency");
        $this->currency = CrowdFundingCurrency::getInstance(JFactory::getDbo(), $currencyId, $this->cfParams);

        // Add submenu
        CrowdFundingFinanceHelper::addSubmenu($this->getName());

        $this->addToolbar();
        $this->addSidebar();
        $this->setDocument();

        parent::display($tpl);
    }

    /**
     * Add a menu on the sidebar of page
     */
    protected function addSidebar()
    {
        $this->sidebar = JHtmlSidebar::render();
    }

    /**
     * Add the page title and toolbar.
     *
     * @since   1.6
     */
    protected function addToolbar()
    {
        JToolbarHelper::title(JText::_("COM_CROWDFUNDINGFINANCE_DASHBOARD"));

        JToolbarHelper::preferences('com_crowdfundingfinance');
        JToolbarHelper::divider();

        // Help button
        $bar = JToolbar::getInstance('toolbar');
        $bar->appendButton('Link', 'help', JText::_('JHELP'), JText::_('COM_CROWDFUNDINGFINANCE_HELP_URL'));
    }

    /**
     * Method to set up the document properties
     *
     * @return void
     */
    protected function setDocument()
    {
        $this->document->setTitle(JText::_('COM_CROWDFUNDINGFINANCE_DASHBOARD'));

    }
}
