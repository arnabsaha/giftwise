<?php
/*
 * @component AlphaUserPoints
 * @copyright Copyright (C) 2008-2014 Bernard Gilly
 * @license : GNU/GPL
 * @Website : http://www.alphaplug.com
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

Jimport( 'joomla.application.component.view');

class alphauserpointsViewPlugins extends JViewLegacy {

	function show($tpl = null) {
	
		JToolBarHelper::title( 'AlphaUserPoints :: ' . JText::_( 'AUP_PLUGINS' ), 'plugin' );

		JToolBarHelper::back();
		
		getPrefHelpToolbar();		
		
		parent::display( $tpl) ;		
	}
}
?>
