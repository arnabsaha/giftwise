<?php
/*
 * @component AlphaUserPoints
 * @copyright Copyright (C) 2008-2014 Bernard Gilly
 * @license : GNU/GPL
 * @Website : http://www.alphaplug.com
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

Jimport( 'joomla.application.component.view');

class alphauserpointsViewArchive extends JViewLegacy {

	function show($tpl = null) {
		
		$document	=  JFactory::getDocument();
		
		//JFactory::getApplication()->input->set( 'hidemainmenu', 1 );
		
		JToolBarHelper::title( 'AlphaUserPoints :: ' . JText::_('AUP_COMBINE_ACTIVITIES'), 'install' );
		
		getCpanelToolbar();

		JToolBarHelper::back();
		
		getPrefHelpToolbar();
	
		parent::display( $tpl);
		
	}
}
?>
