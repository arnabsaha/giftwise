<?php
/*
 * @component AlphaUserPoints
 * @copyright Copyright (C) 2008-2014 Bernard Gilly
 * @license : GNU/GPL
 * @Website : http://www.alphaplug.com
 */


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );
jimport( 'joomla.html.pagination' );

class alphauserpointsViewLevelrank extends JViewLegacy {

	function _displaylist($tpl = null) {
		
		JToolBarHelper::title( 'AlphaUserPoints :: ' .  JText::_( 'AUP_LEVEL_RANK_MEDALS' ), 'levels' );
		getCpanelToolbar();
		if (JFactory::getUser()->authorise('core.edit', 'com_alphauserpoints')) {
			JToolBarHelper::editList( 'editlevelrank' );
		}
		if (JFactory::getUser()->authorise('core.create', 'com_alphauserpoints')) {
			JToolBarHelper::addNew( 'editlevelrank' );
		}
		if (JFactory::getUser()->authorise('core.delete', 'com_alphauserpoints')) {
			JToolBarHelper::custom( 'deletelevelrank', 'delete.png', 'delete.png', JText::_('AUP_DELETE') );		
		}
		getPrefHelpToolbar();
		
		$pagination = new JPagination( $this->total, $this->limitstart, $this->limit );		
		
		$this->assignRef( 'pagination', $pagination );
		$this->assignRef( 'levelrank', $this->levelrank );
		$this->assignRef( 'lists',  $this->lists );

		parent::display( $tpl) ;
	}
	
	function _edit_levelrank($tpl = null) {
		
		JFactory::getApplication()->input->set( 'hidemainmenu', 1 );
		
		JToolBarHelper::title(  'AlphaUserPoints :: ' . JText::_( 'AUP_LEVEL_RANK_MEDALS' ), 'levels-add' );
		
		getCpanelToolbar();
		if (JFactory::getUser()->authorise('core.edit.state', 'com_alphauserpoints')) {
			JToolBarHelper::save( 'savelevelrank' );
		}
		JToolBarHelper::cancel( 'cancellevelrank' );
		getPrefHelpToolbar();
		
		$this->assignRef( 'row', $this->row );
		$this->assignRef( 'lists', $this->lists );
		
		parent::display( "form" ) ;
	}
	
	
	function  _displaydetailrank($tpl = null) {

		JToolBarHelper::title( 'AlphaUserPoints :: ' .  JText::_( 'AUP_AWARDED' ), 'addedit' );
		getCpanelToolbar();
		JToolBarHelper::back();
		getPrefHelpToolbar();
		
		$this->assignRef( 'detailrank', $this->detailrank );
	
		$pagination = new JPagination( $this->total, $this->limitstart, $this->limit );		
		$this->assignRef( 'pagination', $pagination );
		
		parent::display( "listing" );
	}
}
?>
